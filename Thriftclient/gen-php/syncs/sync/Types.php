<?php
namespace syncs\sync;

/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;


class Product {
  static $_TSPEC;

  /**
   * @var string
   */
  public $code = null;
  /**
   * @var string
   */
  public $name = null;
  /**
   * @var string
   */
  public $categoryName = null;
  /**
   * @var double
   */
  public $price_sell = null;
  /**
   * @var double
   */
  public $price_buy = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'code',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'name',
          'type' => TType::STRING,
          ),
        3 => array(
          'var' => 'categoryName',
          'type' => TType::STRING,
          ),
        4 => array(
          'var' => 'price_sell',
          'type' => TType::DOUBLE,
          ),
        5 => array(
          'var' => 'price_buy',
          'type' => TType::DOUBLE,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['code'])) {
        $this->code = $vals['code'];
      }
      if (isset($vals['name'])) {
        $this->name = $vals['name'];
      }
      if (isset($vals['categoryName'])) {
        $this->categoryName = $vals['categoryName'];
      }
      if (isset($vals['price_sell'])) {
        $this->price_sell = $vals['price_sell'];
      }
      if (isset($vals['price_buy'])) {
        $this->price_buy = $vals['price_buy'];
      }
    }
  }

  public function getName() {
    return 'Product';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->code);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->name);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->categoryName);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 4:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->price_sell);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 5:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->price_buy);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('Product');
    if ($this->code !== null) {
      $xfer += $output->writeFieldBegin('code', TType::STRING, 1);
      $xfer += $output->writeString($this->code);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->name !== null) {
      $xfer += $output->writeFieldBegin('name', TType::STRING, 2);
      $xfer += $output->writeString($this->name);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->categoryName !== null) {
      $xfer += $output->writeFieldBegin('categoryName', TType::STRING, 3);
      $xfer += $output->writeString($this->categoryName);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->price_sell !== null) {
      $xfer += $output->writeFieldBegin('price_sell', TType::DOUBLE, 4);
      $xfer += $output->writeDouble($this->price_sell);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->price_buy !== null) {
      $xfer += $output->writeFieldBegin('price_buy', TType::DOUBLE, 5);
      $xfer += $output->writeDouble($this->price_buy);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class OrderItem {
  static $_TSPEC;

  /**
   * @var string
   */
  public $productCode = null;
  /**
   * @var double
   */
  public $price = null;
  /**
   * @var double
   */
  public $multiply = null;
  /**
   * @var double
   */
  public $amount = null;
  /**
   * @var double
   */
  public $discount = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'productCode',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'price',
          'type' => TType::DOUBLE,
          ),
        3 => array(
          'var' => 'multiply',
          'type' => TType::DOUBLE,
          ),
        4 => array(
          'var' => 'amount',
          'type' => TType::DOUBLE,
          ),
        5 => array(
          'var' => 'discount',
          'type' => TType::DOUBLE,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['productCode'])) {
        $this->productCode = $vals['productCode'];
      }
      if (isset($vals['price'])) {
        $this->price = $vals['price'];
      }
      if (isset($vals['multiply'])) {
        $this->multiply = $vals['multiply'];
      }
      if (isset($vals['amount'])) {
        $this->amount = $vals['amount'];
      }
      if (isset($vals['discount'])) {
        $this->discount = $vals['discount'];
      }
    }
  }

  public function getName() {
    return 'OrderItem';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->productCode);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->price);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->multiply);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 4:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->amount);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 5:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->discount);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('OrderItem');
    if ($this->productCode !== null) {
      $xfer += $output->writeFieldBegin('productCode', TType::STRING, 1);
      $xfer += $output->writeString($this->productCode);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->price !== null) {
      $xfer += $output->writeFieldBegin('price', TType::DOUBLE, 2);
      $xfer += $output->writeDouble($this->price);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->multiply !== null) {
      $xfer += $output->writeFieldBegin('multiply', TType::DOUBLE, 3);
      $xfer += $output->writeDouble($this->multiply);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->amount !== null) {
      $xfer += $output->writeFieldBegin('amount', TType::DOUBLE, 4);
      $xfer += $output->writeDouble($this->amount);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->discount !== null) {
      $xfer += $output->writeFieldBegin('discount', TType::DOUBLE, 5);
      $xfer += $output->writeDouble($this->discount);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class Order {
  static $_TSPEC;

  /**
   * @var string
   */
  public $id = null;
  /**
   * @var \syncs\sync\OrderItem[]
   */
  public $items = null;
  /**
   * @var double
   */
  public $amount = null;
  /**
   * @var double
   */
  public $actualAmount = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'id',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'items',
          'type' => TType::LST,
          'etype' => TType::STRUCT,
          'elem' => array(
            'type' => TType::STRUCT,
            'class' => '\syncs\sync\OrderItem',
            ),
          ),
        3 => array(
          'var' => 'amount',
          'type' => TType::DOUBLE,
          ),
        4 => array(
          'var' => 'actualAmount',
          'type' => TType::DOUBLE,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['id'])) {
        $this->id = $vals['id'];
      }
      if (isset($vals['items'])) {
        $this->items = $vals['items'];
      }
      if (isset($vals['amount'])) {
        $this->amount = $vals['amount'];
      }
      if (isset($vals['actualAmount'])) {
        $this->actualAmount = $vals['actualAmount'];
      }
    }
  }

  public function getName() {
    return 'Order';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->id);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::LST) {
            $this->items = array();
            $_size0 = 0;
            $_etype3 = 0;
            $xfer += $input->readListBegin($_etype3, $_size0);
            for ($_i4 = 0; $_i4 < $_size0; ++$_i4)
            {
              $elem5 = null;
              $elem5 = new \syncs\sync\OrderItem();
              $xfer += $elem5->read($input);
              $this->items []= $elem5;
            }
            $xfer += $input->readListEnd();
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->amount);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 4:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->actualAmount);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('Order');
    if ($this->id !== null) {
      $xfer += $output->writeFieldBegin('id', TType::STRING, 1);
      $xfer += $output->writeString($this->id);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->items !== null) {
      if (!is_array($this->items)) {
        throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
      }
      $xfer += $output->writeFieldBegin('items', TType::LST, 2);
      {
        $output->writeListBegin(TType::STRUCT, count($this->items));
        {
          foreach ($this->items as $iter6)
          {
            $xfer += $iter6->write($output);
          }
        }
        $output->writeListEnd();
      }
      $xfer += $output->writeFieldEnd();
    }
    if ($this->amount !== null) {
      $xfer += $output->writeFieldBegin('amount', TType::DOUBLE, 3);
      $xfer += $output->writeDouble($this->amount);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->actualAmount !== null) {
      $xfer += $output->writeFieldBegin('actualAmount', TType::DOUBLE, 4);
      $xfer += $output->writeDouble($this->actualAmount);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class StockCurrentBase {
  static $_TSPEC;

  /**
   * @var string
   */
  public $productCode = null;
  /**
   * @var int
   */
  public $version = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'productCode',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'version',
          'type' => TType::I32,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['productCode'])) {
        $this->productCode = $vals['productCode'];
      }
      if (isset($vals['version'])) {
        $this->version = $vals['version'];
      }
    }
  }

  public function getName() {
    return 'StockCurrentBase';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->productCode);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->version);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('StockCurrentBase');
    if ($this->productCode !== null) {
      $xfer += $output->writeFieldBegin('productCode', TType::STRING, 1);
      $xfer += $output->writeString($this->productCode);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->version !== null) {
      $xfer += $output->writeFieldBegin('version', TType::I32, 2);
      $xfer += $output->writeI32($this->version);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class StockCurrent {
  static $_TSPEC;

  /**
   * @var \syncs\sync\StockCurrentBase
   */
  public $stockBase = null;
  /**
   * @var double
   */
  public $multiply = null;
  /**
   * @var \syncs\sync\Product
   */
  public $product = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'stockBase',
          'type' => TType::STRUCT,
          'class' => '\syncs\sync\StockCurrentBase',
          ),
        2 => array(
          'var' => 'multiply',
          'type' => TType::DOUBLE,
          ),
        3 => array(
          'var' => 'product',
          'type' => TType::STRUCT,
          'class' => '\syncs\sync\Product',
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['stockBase'])) {
        $this->stockBase = $vals['stockBase'];
      }
      if (isset($vals['multiply'])) {
        $this->multiply = $vals['multiply'];
      }
      if (isset($vals['product'])) {
        $this->product = $vals['product'];
      }
    }
  }

  public function getName() {
    return 'StockCurrent';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRUCT) {
            $this->stockBase = new \syncs\sync\StockCurrentBase();
            $xfer += $this->stockBase->read($input);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->multiply);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::STRUCT) {
            $this->product = new \syncs\sync\Product();
            $xfer += $this->product->read($input);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('StockCurrent');
    if ($this->stockBase !== null) {
      if (!is_object($this->stockBase)) {
        throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
      }
      $xfer += $output->writeFieldBegin('stockBase', TType::STRUCT, 1);
      $xfer += $this->stockBase->write($output);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->multiply !== null) {
      $xfer += $output->writeFieldBegin('multiply', TType::DOUBLE, 2);
      $xfer += $output->writeDouble($this->multiply);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->product !== null) {
      if (!is_object($this->product)) {
        throw new TProtocolException('Bad type in structure.', TProtocolException::INVALID_DATA);
      }
      $xfer += $output->writeFieldBegin('product', TType::STRUCT, 3);
      $xfer += $this->product->write($output);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

class Closedcash {
  static $_TSPEC;

  /**
   * @var string
   */
  public $id = null;
  /**
   * @var int
   */
  public $datestart = null;
  /**
   * @var int
   */
  public $dateend = null;
  /**
   * @var double
   */
  public $total = null;
  /**
   * @var int
   */
  public $payCount = null;
  /**
   * @var int
   */
  public $refundCount = null;
  /**
   * @var double
   */
  public $payTotal = null;
  /**
   * @var double
   */
  public $refundTotal = null;
  /**
   * @var int
   */
  public $dutySequence = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'id',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'datestart',
          'type' => TType::I64,
          ),
        3 => array(
          'var' => 'dateend',
          'type' => TType::I64,
          ),
        4 => array(
          'var' => 'total',
          'type' => TType::DOUBLE,
          ),
        5 => array(
          'var' => 'payCount',
          'type' => TType::I32,
          ),
        6 => array(
          'var' => 'refundCount',
          'type' => TType::I32,
          ),
        7 => array(
          'var' => 'payTotal',
          'type' => TType::DOUBLE,
          ),
        8 => array(
          'var' => 'refundTotal',
          'type' => TType::DOUBLE,
          ),
        9 => array(
          'var' => 'dutySequence',
          'type' => TType::I32,
          ),
        );
    }
    if (is_array($vals)) {
      if (isset($vals['id'])) {
        $this->id = $vals['id'];
      }
      if (isset($vals['datestart'])) {
        $this->datestart = $vals['datestart'];
      }
      if (isset($vals['dateend'])) {
        $this->dateend = $vals['dateend'];
      }
      if (isset($vals['total'])) {
        $this->total = $vals['total'];
      }
      if (isset($vals['payCount'])) {
        $this->payCount = $vals['payCount'];
      }
      if (isset($vals['refundCount'])) {
        $this->refundCount = $vals['refundCount'];
      }
      if (isset($vals['payTotal'])) {
        $this->payTotal = $vals['payTotal'];
      }
      if (isset($vals['refundTotal'])) {
        $this->refundTotal = $vals['refundTotal'];
      }
      if (isset($vals['dutySequence'])) {
        $this->dutySequence = $vals['dutySequence'];
      }
    }
  }

  public function getName() {
    return 'Closedcash';
  }

  public function read($input)
  {
    $xfer = 0;
    $fname = null;
    $ftype = 0;
    $fid = 0;
    $xfer += $input->readStructBegin($fname);
    while (true)
    {
      $xfer += $input->readFieldBegin($fname, $ftype, $fid);
      if ($ftype == TType::STOP) {
        break;
      }
      switch ($fid)
      {
        case 1:
          if ($ftype == TType::STRING) {
            $xfer += $input->readString($this->id);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 2:
          if ($ftype == TType::I64) {
            $xfer += $input->readI64($this->datestart);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 3:
          if ($ftype == TType::I64) {
            $xfer += $input->readI64($this->dateend);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 4:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->total);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 5:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->payCount);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 6:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->refundCount);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 7:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->payTotal);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 8:
          if ($ftype == TType::DOUBLE) {
            $xfer += $input->readDouble($this->refundTotal);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        case 9:
          if ($ftype == TType::I32) {
            $xfer += $input->readI32($this->dutySequence);
          } else {
            $xfer += $input->skip($ftype);
          }
          break;
        default:
          $xfer += $input->skip($ftype);
          break;
      }
      $xfer += $input->readFieldEnd();
    }
    $xfer += $input->readStructEnd();
    return $xfer;
  }

  public function write($output) {
    $xfer = 0;
    $xfer += $output->writeStructBegin('Closedcash');
    if ($this->id !== null) {
      $xfer += $output->writeFieldBegin('id', TType::STRING, 1);
      $xfer += $output->writeString($this->id);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->datestart !== null) {
      $xfer += $output->writeFieldBegin('datestart', TType::I64, 2);
      $xfer += $output->writeI64($this->datestart);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->dateend !== null) {
      $xfer += $output->writeFieldBegin('dateend', TType::I64, 3);
      $xfer += $output->writeI64($this->dateend);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->total !== null) {
      $xfer += $output->writeFieldBegin('total', TType::DOUBLE, 4);
      $xfer += $output->writeDouble($this->total);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->payCount !== null) {
      $xfer += $output->writeFieldBegin('payCount', TType::I32, 5);
      $xfer += $output->writeI32($this->payCount);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->refundCount !== null) {
      $xfer += $output->writeFieldBegin('refundCount', TType::I32, 6);
      $xfer += $output->writeI32($this->refundCount);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->payTotal !== null) {
      $xfer += $output->writeFieldBegin('payTotal', TType::DOUBLE, 7);
      $xfer += $output->writeDouble($this->payTotal);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->refundTotal !== null) {
      $xfer += $output->writeFieldBegin('refundTotal', TType::DOUBLE, 8);
      $xfer += $output->writeDouble($this->refundTotal);
      $xfer += $output->writeFieldEnd();
    }
    if ($this->dutySequence !== null) {
      $xfer += $output->writeFieldBegin('dutySequence', TType::I32, 9);
      $xfer += $output->writeI32($this->dutySequence);
      $xfer += $output->writeFieldEnd();
    }
    $xfer += $output->writeFieldStop();
    $xfer += $output->writeStructEnd();
    return $xfer;
  }

}

final class Constant extends \Thrift\Type\TConstant {
  static protected $SERVICENAMESPACE;

  static protected function init_SERVICENAMESPACE() {
    return "sync";
  }
}


