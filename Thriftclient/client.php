<?php
require_once '../Thriftclient/lib/Thrift/ClassLoader/ThriftClassLoader.php';
//require_once IA_ROOT.'/Thriftclient/lib/Thrift/Protocol/TBinaryProtocol.php';
//require_once IA_ROOT.'/Thriftclient/lib/Thrift/Transport/TSocket.php';
//require_once IA_ROOT.'/Thriftclient/lib/Thrift/Transport/TSocketPool.php';
//require_once IA_ROOT.'/Thriftclient/lib/Thrift/Transport/TFramedTransport.php';
///require_once IA_ROOT.'/Thriftclient/lib/Thrift/Transport/TBufferedTransport.php';
////require_once IA_ROOT.'/Thriftclient/lib/Thrift/Protocol/TCompactProtocol.php';
//require_once IA_ROOT.'/Thriftclient/lib/Thrift/Protocol/TMultiplexedProtocol.php';
//require_once IA_ROOT.'/Thriftclient/lib/Thrift/Exception/TException.php';

use Thrift\ClassLoader\ThriftClassLoader;
use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TSocket;
use Thrift\Transport\TSocketPool;
use Thrift\Transport\TFramedTransport;
use Thrift\Transport\TBufferedTransport;
use Thrift\Protocol\TCompactProtocol;
use Thrift\Protocol\TMultiplexedProtocol;
use Thrift\Exception\TException;
$GEN_DIR = realpath(dirname(__FILE__)).'/gen-php';
$LIB_DIR = realpath(dirname(__FILE__)).'/lib';
$loader = new ThriftClassLoader();
$loader->registerNamespace('Thrift', $LIB_DIR); # 加载thrift
$loader->registerDefinition('syncs\channel', $GEN_DIR); # 加载自己写的thrift文件编译的类文件和数据定义  相当于include的作用
$loader->registerDefinition('syncs\exceptions', $GEN_DIR); # 加载自己写的thrift文件编译的类文件和数据定义
$loader->registerDefinition('syncs\notice', $GEN_DIR); # 加载自己写的thrift文件编译的类文件和数据定义
$loader->registerDefinition('syncs\pos', $GEN_DIR); # 加载自己写的thrift文件编译的类文件和数据定义
$loader->registerDefinition('syncs\shared', $GEN_DIR); # 加载自己写的thrift文件编译的类文件和数据定义
$loader->registerDefinition('syncs\sync', $GEN_DIR); # 加载自己写的thrift文件编译的类文件和数据定义
$loader->register();
?>
