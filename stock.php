<?php
define('IA_ROOT', str_replace("\\", '/', dirname(__FILE__)));
define('IN_IA', true);
define('TIMESTAMP', time());
$_W = $_GPC = array();
$configfile = IA_ROOT . "/data/config.php";

require $configfile;
require IA_ROOT . '/framework/version.inc.php';
require IA_ROOT . '/framework/const.inc.php';
require IA_ROOT . '/framework/class/loader.class.php';
load()->func('global');
load()->func('compat');
load()->func('pdo');//func加载方法
load()->classs('account');
load()->classs('agent');
load()->model('cache');
load()->model('account');
//load()->model('setting');//$_W['setting']  model加载数据库数据

define('CLIENT_IP', getip());

$_W['config'] = $config;
$_W['config']['db']['tablepre'] = !empty($_W['config']['db']['master']['tablepre']) ? $_W['config']['db']['master']['tablepre'] : $_W['config']['db']['tablepre'];
$_W['timestamp'] = TIMESTAMP;
$_W['charset'] = $_W['config']['setting']['charset'];
$_W['clientip'] = CLIENT_IP;

unset($configfile, $config);


define('DEVELOPMENT', $_W['config']['setting']['development'] == 1);
if(DEVELOPMENT) {
	ini_set('display_errors', '1');
	error_reporting(E_ALL ^ E_NOTICE);
} else {
	error_reporting(0);
}
if(!in_array($_W['config']['setting']['cache'], array('mysql', 'file', 'memcache'))) {
	$_W['config']['setting']['cache'] = 'mysql';
}
load()->func('cache');

if(function_exists('date_default_timezone_set')) {
	date_default_timezone_set($_W['config']['setting']['timezone']);
}
if(!empty($_W['config']['memory_limit']) && function_exists('ini_get') && function_exists('ini_set')) {
	if(@ini_get('memory_limit') != $_W['config']['memory_limit']) {
		@ini_set('memory_limit', $_W['config']['memory_limit']);
	}
}

$input = file_get_contents("php://input");
//file_put_contents("stock.txt", $input);
if (empty($input)) {
	header('HTTP/1.1 500 Internal Server Error');exit;
}
else
{
	$dinput = @json_decode($input, true);
	$dinput = ihtmlspecialchars($dinput);       //防止参数部分符号的sql注入，单引号等
	//file_put_contents("text.txt", $dinput['quantity']."-".$dinput['storeCode']."-".$dinput['productCode']);
	if(empty($dinput['quantity'])||empty($dinput['storeCode'])||empty($dinput['productCode']))
	{
		header("HTTP/1.0 400 Bad Request");exit;
	}
	else{
		$up = array('stock' => $dinput['quantity']);
		$gup = array('total' => $dinput['quantity']);
		$tt1=pdo_update('ims_ewei_shop_goods_option', $up, array(
                'storecode' =>$dinput['storeCode'],
                'goodssn' => $dinput['productCode']));
		$tt2=pdo_update('ims_ewei_shop_goods', $gup, array(
				'storecode' =>$dinput['storeCode'],
				'goodssn' => $dinput['productCode']));
		if(!empty($tt1)||!empty($tt2))
		{
			header("HTTP/1.0 200 OK");exit;
		}
		else 
		{
			$r=print_r($dinput,true);
			//file_put_contents("stock.txt", $r,FILE_APPEND);
			header("HTTP/1.0 404 Not Found");exit;
		}
	}
}


?>