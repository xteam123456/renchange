<?php
function redata($code,$mess)
{
    return json_encode(array('code'=>$code,'mess'=>$mess));
}
function mkdirs($path) {
    if (!is_dir($path)) {
        mkdirs(dirname($path));
        mkdir($path);
    }
    return is_dir($path);
}


if(empty($_GPC['i']))
{
    $_W['uniacid']=22;
}
else
{
    $_W['uniacid'] = intval($_GPC['i']);
}
if($_GPC['need']){
    $gethead=getallheaders();
    $token=$gethead['token'];
    if(empty($token))
    {
        http_response_code(401);
        echo json_encode(array("error" => "您没登录, 请登录后访问"));
        exit;
    }
    else
    {
        $sql="select uid,logintime from ".tablename("mc_members")." where token=:token and uniacid=:uniacid ";
        $tkmc=pdo_fetch($sql,array(':token'=>$token,':uniacid'=>$_W['uniacid']));
        $uid=$tkmc['uid'];
        if($uid>0)
        {
            $ltime=intval($tkmc['logintime']);
          /*  if($ltime<time()-1800)
            {
                http_response_code(401);
                echo json_encode(array("error" => "token无效"));
                exit;
            }*/
            $sql="select tb2.* from ".tablename("mc_mapping_fans")." as tb1 ";
            $sql.=" join ".tablename("sz_yi_member"). " as tb2 on tb1.openid=tb2.openid ";
            $sql.=" where tb1.uid=:uid and tb1.uniacid=:uniacid  limit 1";
            $webmember=pdo_fetch($sql,array(":uid"=>$uid,':uniacid'=>$_W['uniacid']));
           // echo json_encode( $webmember);
            if(empty($webmember))
            { 
                http_response_code(401);
                echo json_encode(array("error" => "用户不存在"));
            }
            $_W['webmember']=$webmember;
            $_W['isajax']=1;
            pdo_update('mc_members',array('logintime'=>time()),array('uid'=>$uid));            
        }
        else
        {
            http_response_code(401);
            echo json_encode(array("error" => "token无效"));
            exit;
        }
    }
}
