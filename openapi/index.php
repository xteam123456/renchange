<?php

// Kickstart the framework
$f3 = require('lib/base.php');

$f3->set('DEBUG', 1);
if ((float)PCRE_VERSION < 7.9)
    trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('config.ini');

$db = new \DB\SQL(
    $f3->get('DB_URL'),
    $f3->get('DB_USER'),
    $f3->get('DB_PASSWORD')
);

class Framework
{
    private $f3;

    function __construct()
    {
        global $f3;
        $this->f3 = $f3;
    }

    function get($name)
    {
        return $this->f3->get($name);
    }
}

class DBase extends Framework
{
    public $db;

    function sysTablename($name)
    {
        return $this->get('TABLE_PREFIX') . $name;
    }

    function tablename($name)
    {
        return $this->get('TABLE_SZ_YI_PREFIX') . $name;
    }

    function __construct()
    {
        parent::__construct();
        global $db;
        $this->db = $db;
    }

    function getDefaultUniacid()
    {
        return $this->db->exec('select uniacid from ' . $this->sysTablename('uni_account') . ' limit 1')[0]['uniacid'];
    }

    function query($args, $fields = '*', $limit = -1, $offset = -1, $orderbyFields, $groupbyFields)
    {

    }

    function mapper($tablename, $fields = NUll, $pureTablename = false)
    {
        return new DB\SQL\Mapper($this->db, $pureTablename ? $tablename : $this->tablename($tablename), $fields);
    }


    function paginate($mapper, $page = 1, $size = 10, $filter = NULL, array $options = NULL, $nocast = false)
    {
        if (empty($filter)) {
            $filter = array("uniacid=?", 6);
        } else {
            $filter[0] = $filter[0] . " and uniacid=?";
            $filter[] = $this->getDefaultUniacid();
        }
        $ret = $mapper->paginate($page - 1, $size, $filter, $options);

        if (!$nocast) {
            $subset = [];
            foreach ($ret['subset'] as $obj) {
                $subset[] = $mapper->cast($obj);
            }
            $ret['subset'] = $subset;
        }
        $ret['pos'] += 1;
        return $ret;
    }


    /**
     * @param $cmds only support string of one sql
     * @param null $args
     * @param bool $haswhere
     * @param int $limit
     * @param null $orderby
     * @param int $offset
     * @return array|FALSE|int
     */
    function execWithUniacid($cmds, $args = NULL, $haswhere = false, $limit = -1, $orderby = null, $offset = -1, $uniacidKey = NULL)
    {
        if (!empty($args)) {
            if (is_array($args)) {
                $args[] = $this->getDefaultUniacid();
            } else {
                $args = array($args, $this->getDefaultUniacid());
            }
        } else {
            $args = array($this->getDefaultUniacid());
        }
        $sql = $cmds;
        $uniacidKey = $uniacidKey == NULL ? 'uniacid' : $uniacidKey;
        if ($haswhere)
            $sql = $cmds . " and $uniacidKey = ?";
        else
            $sql = $cmds . " where $uniacidKey = ?";
        if ($orderby) {
            $sql .= $orderby;
        }
        if ($limit > 0) {
            $sql .= " limit $limit";
        }
        if ($offset > 0) {
            $sql .= ' offset $offset';
        }
        return $this->db->exec($sql, $args);
    }

    function exec($cmds, $args)
    {
        return $this->db->exec($cmds, $args);
    }

}

class Api extends DBase
{
    protected $content_type = "application/json";
    function __construct()
    {
        parent::__construct();
        header('Content-Type: '.$this->content_type);
    }

    private $tablename;
    private $pureTablename = false;

    function blist()
    {
        if (!$this->tablename) {
            http_response_code(500);
            echo "内部错误, 错误代码: 0001";
        }
        $this->paginateX($this->tablename);
    }

    function paginateX($tablename, $filter = NULL, callable $mapper_preprocessor = NULL, array $search_fields=NULL)
    {
        $fields = $this->get('GET.fields') ?  : NULL;
        if ($fields) {
            $fields = explode(',', $fields);
        }
        $page = $this->get('GET.page') ?  : 1;
        $limit = $this->get('GET.limit') ?  : 20;
        $orderby = $this->get('GET.o') ?  : NULL;
        $orderstr = NULL;
        $options = [];
        if ($orderby) {
            $orderbys = explode(',', $orderby);
            $orderstr = "";
            foreach ($orderbys as $val) {
                $val = trim($val);
                if ($val) {
                    if (stripos($val, '-')) {
                        $orderstr .= "$val desc,";
                    } else {
                        $orderstr .= $val . ",";
                    }
                }
            }
            $options['order'] = stripos($orderstr, ',', strlen($orderstr) - 1) == strlen($orderstr) - 1 ? substr($orderstr, 0, strlen($orderstr) - 1) : $orderstr;
        }
        $mapper = $this->mapper($tablename, $fields);
        if ($mapper_preprocessor) {
            $mapper_preprocessor($mapper);
        }
        $unwrap = $this->get('GET.unwrap') ?: 0;
        if ($search_fields) {
            $searchstrarray = [];
            $searchargs = [];
            foreach ($search_fields as $key) {
                if ($this->get("GET.$key")) {
                    $searchstrarray[] = $key. "=?";
                    $searchargs[] = $this->get("GET.$key");
                }
            }
            $searchstr = '';
            if($searchstrarray){
                if(count($searchstrarray) > 1){
                    $searchstr = implode(' and ', $searchstrarray);
                }else{
                    $searchstr = $searchstrarray[0];
                }
            }
            if($searchstr)
            {
                if ($filter) {
                    $filterstr = $filter[0]." and $searchstr";
                    $filter[0] = $filterstr;
                    $filter = array_merge($filter, $searchargs);
                }else{
                    $filter[] = $searchstr;
                    $filter = array_merge($filter, $searchargs);
                }
            }
        }
        if ($unwrap == 1)
            return $this->paginate($mapper, $page, $limit, $filter, $options)['subset'];
        else
            return $this->paginate($mapper, $page, $limit, $filter, $options);
    }
}

class IndexApi extends Api
{

    function banners()
    {
        echo json_encode($this->execWithUniacid('select link, thumb_pc from ' . $this->tablename('adv') . ' where enabled=?', 1, true), true);
    }

    function famousEnterprises()
    {
        echo json_encode($this->execWithUniacid('select id, thumb from ' . $this->tablename('perm_user')), true);
    }

    function _advertisements($limit = -1)
    {
        return $this->execWithUniacid('select thumb, link from ' . $this->tablename('adpc') . ' where enabled=? ', 1, true, $limit);
    }

    function footerAdvertisements()
    {
        echo json_encode($this->_advertisements(3));
    }

    function positionPagination($fields = NULL, $size = 20, $page = 1)
    {
        $gm = $this->mapper('goods', $fields);

        $out = $gm->paginate(($page - 1) * $size, $size);

        $subset = [];
        foreach ($out['subset'] as $obj) {
            $subset[] = $gm->cast($obj);
        }
        $out['subset'] = json_encode($subset, true);
//        echo json_encode($out, true);
    }

    function positions()
    {
        $gm = $this->mapper('goods', ['id', 'uniacid', 'status']);
        $out = $this->paginate($gm, 0, 2);
        echo json_encode($out, true);
    }

    private function _record_2_element($record)
    {
        $ret = [];
        $ret['id'] = $record['id'];
        $ret['name'] = $record['name'];
        $ret['cats2'] = array(array('id' => $record['id2'], 'name' => $record['name2']));
        return $ret;
    }

    function indexPositionTree()
    {
        $cats = $this->execWithUniacid('select cat1.id as id, cat1.name as name, cat2.id as id2, cat2.name as name2 from '
            . $this->tablename('category') .
            " as cat1 left join " . $this->tablename('category') . " as cat2 on cat1.id = cat2.parentid " .
            "where cat1.level = 1 and cat2.level=2", NULL, true, -1, ' order by cat1.id', -1, 'cat1.uniacid');
        $ret = [];
        foreach ($cats as $record) {
            if (array_key_exists($record['id'], $ret)) {
                $e = &$ret[$record['id']];
                $e['cats2'][] = array('id' => $record['id2'], 'name' => $record['name2']);
            } else {
                $ret[$record['id']] = $this->_record_2_element($record);
            }
        }
        foreach ($ret as $id => &$record) {
            $record['goods'] = $this->execWithUniacid("select CONCAT_WS('', goods.province, goods.city, goods.area) as address, goods.id as id, goods.marketprice as marketprice, goods.title as title, " .
                "company.realname as compname, company.id as compid, company.logo as complogo from " . $this->tablename('goods') . " as goods left join " . $this->tablename('perm_user')
                . " as company on company.uid = goods.supplier_uid where goods.status = 1 and goods.pcate =?", array($id), true,
                6, ' order by goods.displayorder desc', -1, 'goods.uniacid');
        }
        echo json_encode(array_values($ret), true);
    }
}

class BusinessApi extends Api
{
    function positionCategories()
    {
        $level = $this->get('GET.level') ?  : 1;
        echo json_encode($this->paginateX('category', array('level=? and mtype=?', $level, 1)));
    }

    function advertisements()
    {
        echo json_encode($this->paginateX('adpc', array('enabled=?', 1)), true);
//        return $this->execWithUniacid('select thumb, link from ' . $this->tablename('adpc') . ' where enabled=? ', 1, true, $limit);
    }
}

class PositionApi extends Api
{
    function search()
    {
        $title = $this->get('GET.title') ?  : NULL;
        $pcate = $this->get('GET.pcate') ? : NULL;
        $xsal = $this->get('GET.xsal') ?  : NULL;
        $msal = $this->get('GET.msal') ?  : NULL;
        $filter = NULL;
        $filterstr = "";
        $filterArgs = array();
        if ($title) {
            $filterstr .= empty($filterstr) ? " title like ? " : " and title like ?";
            $filterArgs[] = "%$title%";
        }
        if ($pcate) {
            $filterstr .= empty($filterstr) ? " pcate = ? " : " and pcate = ?";
            $filterArgs[] = $pcate;
        }
        if ($xsal) {
            $filterstr .= empty($filterstr) ? " marketprice <= ? " : " and marketprice <= ?";
            $filterArgs[] = $xsal * 10000;
        }
        if ($msal) {
            $filterstr .= empty($filterstr) ? " marketprice >= ? " : " and marketprice >= ?";
            $filterArgs[] = $msal * 10000;
        }
        if (!empty($filterstr)) {
            $filter = array_merge([$filterstr], $filterArgs);
        }

        $add_virtual_fields = function ($mapper) {
            $mapper->companyname = 'select realname from ' . $this->tablename('perm_user') .
                " as company where company.uid = " . $this->tablename("goods") . ".supplier_uid";
            $mapper->pcatename = 'select name from ' . $this->tablename('category') . " as category where category.id = " .
                $this->tablename("goods") . ".pcate";
        };
        echo json_encode($this->paginateX('goods', $filter, $add_virtual_fields, array("supplier_uid")), true);
    }

    function load()
    {
        $id = $this->get('PARAMS.id');
        $position = $this->execWithUniacid('select * from ' . $this->tablename('goods') . " where id=?", $id, true, 1)[0];
        $entprise = (new EnterpriseApi())->loadByUid($position['supplier_uid']);
        $position['entprise'] = $entprise;
        echo json_encode($position, true);
    }

}


class TrainingApi extends Api
{
    function trainings()
    {

    }
}

class AuthApi extends Api
{
    protected $user;

    function unauthed()
    {
        http_response_code(401);
        echo json_encode(array("error" => "您没登录, 请登录后访问"));
        exit;
    }

    function __construct()
    {
        parent::__construct();
        $token = $this->get('HEADERS.X-Token');
        if ($token) {
            $this->user = $this->exec("select uid, mobile, realname, nickname from " .
                $this->systablename('mc_members') . " where token = ?", $token);
            $this->user = $this->user[0];
            if ($this->user) {
                $member = $this->exec("select id from " . $this->tablename('member') . " where uid = ?", $this->user['uid']);
                $this->user['id'] = $member[0]['id'];
                $this->user['token'] = $token;
            } else {
                $this->unauthed();
            }
        } else {
            $this->unauthed();
        }

    }
}

class UserApi extends Api
{
}

class LoginApi extends Api
{

    function login()
    {
        $tablename = $this->sysTablename('mc_members');
        $mobile = $this->get('POST.mobile');
        $password = $this->get('POST.password');
        $user = $this->exec('select uid, mobile, password, salt, nickname, realname from ' . $tablename .
            " where mobile = ?", $mobile);
        if ($user) {
            $user = $user[0];
            $conflict = md5($user['salt'].$password);
            if ($conflict == $user['password']) {
                $token = md5(uniqid().$mobile.$password);
                $row = $this->exec("update $tablename set token = ?, logintime = ? where uid = ?", array($token, time(), $user['uid']));
                if ($row) {
                    // login success
                    echo json_encode(array("mobile" => $mobile, "realname" => $user['realname'], "token" => $token));
                    exit;
                }
            }
        }
        http_response_code(401);
        echo json_encode(array("error" => "用户名或密码不对"));
    }

    function registerQrcode()
    {
        include '../framework/library/qrcode/phpqrcode.php';
        $host = $this->get("HOST");
        $url = "http://$host/app/index.php?i=" . $this->getDefaultUniacid() . "&c=entry&p=info&do=member&m=ewei_shop";
        $dir = '../attachment/registerqrcodes';
        if(!is_dir($dir))
            mkdir('../attachment/registerqrcodes');
        $savepath = md5('roeasy').".png";
        QRcode :: png($url, $dir."/".$savepath, QR_ECLEVEL_H, 4);
        echo json_encode(["http://$host/attachment/registerqrcodes/$savepath"]);
//        header('Content-Type: '.'text/html');
//        echo "<image src='$access_url' style='position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);'></image>";
    }

}


class ResumeApi extends AuthApi
{

    function my()
    {
        $baseinfo = $this->exec("select name, worktime, gender, birth, workstaus as workstatus, email, mobile from " . $this->sysTablename('resume_personinfo') . " where memberid = ?", $this->user['id']);
        $baseinfo = $baseinfo[0];
        $educations = $this->exec("select * from " . $this->sysTablename("resume_education") . " where memberid = ?", $this->user['id']);
        $baseinfo['educations'] = $educations;
        $experiences = $this->exec("select * from " . $this->sysTablename("resume_experience") . " where memberid = ?", $this->user['id']);
        $baseinfo['experiences'] = $experiences;
        echo json_encode($baseinfo, true);
    }
}


class EnterpriseApi extends Api
{
    private $fields = ['id', 'uid', 'realname', 'mobile', 'thumb', 'content', 'createtime', 'contacts', 'address', 'logo'];
    function loadById($id)
    {
        return $this->execWithUniacid('select '.implode(',', $this->fields).' from ' . $this->tablename('perm_user') . " where id=?", $id, true, 1)[0];
    }

    function loadByUid($uid)
    {
        return $this->execWithUniacid('select '.implode(',', $this->fields).' from ' . $this->tablename('perm_user') . " where uid=?", $uid, true, 1)[0];

    }

    function load()
    {
        $id = $this->get('PARAMS.id');
        echo json_encode($this->loadById($id), true);
    }

}

class LogoutApi extends AuthApi
{
    function logout()
    {
        if($this->exec('update ' . $this->sysTablename('mc_members') . ' set token = NULL, logintime = NULL where token=?', array($this->user['token'])));
        echo json_encode(array("code"=>200,"messsage"=>“退出成功”));
    }
}

$f3->route('GET /index/banners', 'IndexApi->banners');
$f3->route('GET /index/enterprises/famous', 'IndexApi->famousEnterprises');
$f3->route('GET /index/advertisements/footer', 'IndexApi->footerAdvertisements');
$f3->route('GET /index/indexpositiontree', 'IndexApi->indexPositionTree');
$f3->route('GET /categories', 'BusinessApi->positionCategories');
$f3->route('GET /positions', 'PositionApi->search');
$f3->route('GET /advertisements', 'BusinessApi->advertisements');
$f3->route('GET /resume/my', 'ResumeApi->my');
$f3->route('POST /login', 'LoginApi->login');
$f3->route('GET /register/qrcode', 'LoginApi->registerQrcode');
$f3->route('GET /entprises/@id', 'EnterpriseApi->load');
$f3->route('GET /positions/@id', 'PositionApi->load');
$f3->route('DELETE /logout', 'LogoutApi->logout');
$f3->run();
