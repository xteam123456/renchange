<?php
    global $_GPC, $_W;
    $rid = intval($_GPC['rid']);
    if (empty($rid)) 
    {
        message('抱歉，坐标不存在或是已经删除！', '', 'error');
    }

    $item = pdo_fetch("SELECT * FROM ".tablename('yhc_onenavi')." WHERE rid = :rid ORDER BY `id` DESC", array(':rid' => $rid));
    if (empty($item))
    {
        message('抱歉，坐标不存在或是已经删除！', '', 'error');
    }

    $res = json_decode($this->httpGet("http://apis.map.qq.com/ws/coord/v1/translate?locations=".$item["lat"].",".$item["lng"]."&type=3&key=PMYBZ-MA7RJ-MRAFX-FFB5E-IZFNQ-WPFJ7") , true);

    $url = "http://apis.map.qq.com/uri/v1/routeplan?type=drive&fromcoord=CurrentLocation&to=".$item['title']."&tocoord=".$res["locations"][0]["lat"].",".$res["locations"][0]["lng"]."&policy=1&referer=tengxun";


    header('Location: '. $url);
    exit;

?>
