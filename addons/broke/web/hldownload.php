<?php
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
if (PHP_SAPI == 'cli') die('This example should only be run from a Web Browser');
require_once '../framework/library/phpexcel/PHPExcel.php';

if($_GPC['op']=='customer'){
	$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid  ",array(':weid' => $_W['weid']),'id');
	$pstatus=$this->ProcessStatus();

	$loupanid = intval($_GPC['loupan']);
	$statusid = intval($_GPC['status']);

	if(!empty($loupanid)){
		$where.=" AND loupan={$loupanid} ";
	}
	if($statusid<1000){
		$where.=" AND status={$statusid} ";
	}
	if(!empty($_GPC['outputdate'])){
		$starttime=strtotime($_GPC['outputdate']['start']);
		$endtime=strtotime($_GPC['outputdate']['end'])+3600*24;
		$where.=" AND  createtime>{$starttime}  AND  createtime<{$endtime}";
	}else{
		$starttime=strtotime(date('Y-m-d',TIMESTAMP));
		$endtime=TIMESTAMP;
		$where.=" AND  createtime>{$starttime}  ";
	}


	$cfg = $this->module['config'];
	$protectdate = intval($cfg['protectdate']);
	if($protectdate){
		$protectdate=TIMESTAMP - (3600*24*$protectdate);
	}
	
	$sql="SELECT *  FROM ".tablename('broke_customer')." WHERE weid = '{$_W['weid']}' ".$where." ORDER BY id desc ";
	$list = pdo_fetchall($sql);
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");


	// Add some data
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'ID')
				->setCellValue('B1', '客户名')
				->setCellValue('C1', '手机')
				->setCellValue('D1', '所属楼盘')
				->setCellValue('E1', '分配时间')
				->setCellValue('F1', '状态')			
				->setCellValue('G1', '预约')
				->setCellValue('H1', '是否过期')
				->setCellValue('I1', '提交时间');

	$i=2;
	foreach($list as $row){
		$loupanname=$loupan[intval($row['loupan'])]? $loupan[intval($row['loupan'])]['title'] :'';
		$statusname=$pstatus[intval($row['status'])] ? $pstatus[intval($row['status'])]:'';
		
		$objPHPExcel->setActiveSheetIndex(0)			
				->setCellValue('A'.$i, $row['id'])
				->setCellValue('B'.$i, $row['realname'])
				->setCellValue('C'.$i, $row['mobile'])
				->setCellValue('D'.$i, $loupanname)
				->setCellValue('E'.$i, $row['allottime']>0?date('Y/m/d H:i:s',$row['allottime']):'')
				->setCellValue('F'.$i, $statusname)
				->setCellValue('G'.$i, $row['flag']==0?'预约':'提交')
				->setCellValue('H'.$i, ($protectdate>$row['updatetime']) ? '是':'否')
				->setCellValue('I'.$i, $row['createtime']>0?date('Y/m/d H:i:s',$row['createtime']):'');
		$i++;		
	}					
	$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20); 

	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle(date('Y-m-d',$starttime).'_'.date('Y-m-d',$endtime));


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="customer'.$_W['weid'].'_'.date('Ymd',$starttime).'_'.date('Ymd',$endtime).'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}
if($_GPC['op']=='broke'){

	$identity = pdo_fetchall('SELECT id,identity_name FROM '.tablename('broke_identity')." WHERE `weid` = :weid  ",array(':weid' => $_W['weid']),'id');

	if(!empty($_GPC['outputdate']) ){
		$starttime=strtotime($_GPC['outputdate']['start']);
		$endtime=strtotime($_GPC['outputdate']['end'])+3600*24;
		$where.=" AND  createtime>{$starttime}  AND  createtime<{$endtime}";
	}else{
		$starttime=strtotime(date('Y-m-d',TIMESTAMP));
		$endtime=TIMESTAMP;
		$where.=" AND  createtime>{$starttime} ";
	}
	$sql="SELECT *  FROM ".tablename('broke_member')." WHERE weid = '{$_W['weid']}' ".$where." ORDER BY id desc ";
	
	$list = pdo_fetchall($sql);
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");


	// Add some data
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'ID')
				->setCellValue('B1', '姓名')
				->setCellValue('C1', '手机')
				->setCellValue('D1', '银行卡')
				->setCellValue('E1', '公司')
				->setCellValue('F1', '加入时间')			
				->setCellValue('G1', '已结佣金')
				->setCellValue('H1', '身份');
			

	$i=2;
	foreach($list as $row){
		
		
		$identityname=$identity[intval($row['identity'])]? $identity[intval($row['identity'])]['identity_name'] :'';
		$objPHPExcel->setActiveSheetIndex(0)			
				->setCellValue('A'.$i, $row['id'])
				->setCellValue('B'.$i, $row['realname'])
				->setCellValue('C'.$i, $row['mobile'])
				->setCellValue('D'.$i, $row['bankcard'])
				->setCellValue('E'.$i, $row['company'])
				->setCellValue('F'.$i, date('Y-m-d H:i',$row['createtime']))
				->setCellValue('G'.$i, $row['commission'])
				->setCellValue('H'.$i, $identityname);
		$i++;		
	}					
	$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18); 

	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle(date('Y-m-d',$starttime).'_'.date('Y-m-d',$endtime));


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="customer'.$_W['weid'].'_'.date('Ymd',$starttime).'_'.date('Ymd',$endtime).'.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}

	