<?php
/**
 * 全民经济人模块定义
 */
defined('IN_IA') or exit('Access Denied');
class BrokeModuleSite extends WeModuleSite {
	
	//全民经济人首页
	public function doMobileIndex(){
		global $_W,$_GPC;
		
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user = $_W['openid'];	
		$day_cookies = 1;	
		$shareid = 'broke_shareid'.$_W['uniacid'];
		if(empty($_COOKIE[$shareid]) || (($_GPC['id']!=$_COOKIE[$shareid]) && !empty($_GPC['id']))){
			setcookie("$shareid", $_GPC['id'], time()+3600*24*30);
		}
		if(!empty($from_user)){
			$oauth_openid = "brokeopenid".$_W['uniacid'];
			$urlcookie = "broke_url".$_W['uniacid'];
			if (empty($_COOKIE[$oauth_openid])) {
				$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
				$url = substr($url, 0, strrpos($url, '&'));
				setcookie($urlcookie, $url, time()+3600*240);
				$this->CheckCookie();
			} else {
				if(!empty($_COOKIE[$urlcookie])){
					$url = $_COOKIE[$urlcookie];
					setcookie($urlcookie, '', time()+3600*240);
					header("location:$url");
				}
			}
		}
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $weid));	
		if(!empty($from_user)){
			$sql='SELECT avatar FROM '.tablename('mc_members')." WHERE avatar<>'' AND uid = ".$_W['member']['uid']." LIMIT 1";
			$myheadimg = pdo_fetchcolumn($sql);
			
			if(empty($myheadimg)){
				$myheadimg = '../addons/broke/style/images/header.png';
			}
			$profile = pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			$id = $profile['id'];
			if(intval($id) && $profile['status']==0){
				include $this->template('forbidden');
				exit;
			}
			if(intval($id)){
				$mycustomer= pdo_fetchcolumn('SELECT count(*) FROM '.tablename('broke_customer')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
				$mycommission = pdo_fetchcolumn('SELECT sum(`commission`) FROM '.tablename('broke_commission')." WHERE flag != 2 and  weid = :weid  AND mid = :mid" , array(':weid' => $_W['uniacid'],':mid' => $id));
				$mycommission = !empty($mycommission)?$mycommission:0;
			}
		}
		$loupan = pdo_fetchall('SELECT * FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ORDER BY displayorder DESC",array(':weid' => $_W['uniacid']));
		include $this->template('hlindex');
	}	
	
	//楼盘首页
	public function doMobileLpIndex(){
		
		global $_W,$_GPC;
		$weid = $_W['uniacid'];
		$day_cookies = 1;
		$shareid = 'broke_shareid'.$_W['uniacid'];
		if(empty($_COOKIE[$shareid]) || (($_GPC['id']!=$_COOKIE[$shareid]) && !empty($_GPC['id']))){
			setcookie("$shareid", $_GPC['id'], time()+3600*24*30);
		}
		
		$loupan = pdo_fetchall('SELECT * FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `status` = 1 ORDER BY displayorder DESC",array(':weid' => $_W['uniacid']));
		include $this->template('hllpindex');
	}
	
	//置业顾问
	public function doMobileCounselor(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user=$_W['openid'];	
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		$assistant=pdo_fetch('SELECT * FROM '.tablename('broke_assistant')." WHERE flag = 0 and weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			
		if($_GPC['opp']=='visit'){
			$id = $_GPC['cid'];
			$opp = 'his';
		}else{
			$profile=$assistant;
			$id = $profile['id'];
			if(intval($id) && $profile['status']==0){
				include $this->template('forbidden');
				exit;
			}
		}
		
		$status = $this->ProcessStatus();
		$statuslenth=count($status)-1;
		$cfg = $this->module['config'];
		$statusprotect=count($status)-2;
		$protectdate = intval($cfg['protectdate']);
		
		if($protectdate){
				$protectdate = TIMESTAMP - (3600*24*$protectdate);
				$protectwhere = " AND (updatetime > $protectdate OR status > $statuslenth )";
		}	
		if(intval($id) || $_GPC['opp']=='visit'){
			$pindex = max(1, intval($_GPC['page']));
			$psize = 15;
			if($op=='statussort'){
				$profile=pdo_fetch('SELECT a.*, ac.loupanid FROM '.tablename('broke_assistant')." as a left join ". tablename('broke_acmanager'). " as ac on a.weid = ac.weid and a.code = ac.code WHERE flag = 1 and a.weid = :weid  AND a.from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
				$loupanids = $profile['loupanid'];
				if(empty($profile['loupanid'])){
					$loupanids = 0;
				}
				$s=intval($_GPC['status']);
				if($_GPC['opp']=='visit'){
					if($_GPC['oppp']=='all'){
						$sql="select * from ". tablename('broke_customer'). "where loupan in (".$loupanids.") and weid =". $_W['uniacid']. " $protectwhere ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize;
						$sq="select count(id) from ". tablename('broke_customer'). "where loupan in (".$loupanids.") and weid =". $_W['uniacid']." $protectwhere ";
					}else{
						$sql="select * from ". tablename('broke_customer'). "where loupan in (".$loupanids.") and weid =". $_W['uniacid']." and `status` = ".$s. " ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize;
						$sq="select count(id) from ". tablename('broke_customer'). "where loupan in (".$loupanids.") and weid =". $_W['uniacid']." and `status` = ".$s;
					}
				}else{
					if($_GPC['oppp']=='zyall'){
						$sql="select * from ". tablename('broke_customer'). "where cid =".$id. " and weid =". $_W['uniacid']. " $protectwhere ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize;
						$sq="select count(id) from ". tablename('broke_customer'). "where cid =".$id. " and weid =". $_W['uniacid']."  $protectwhere ";
					}else{
						$sql="select * from ". tablename('broke_customer'). "where cid =".$id. " and weid =". $_W['uniacid']." and `status` = ".$s. " ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize;
						$sq="select count(id) from ". tablename('broke_customer'). "where cid =".$id. " and weid =". $_W['uniacid']." and `status` = ".$s;
					}
				}
				$customer = pdo_fetchall($sql);
				$total = pdo_fetchcolumn($sq);
			
			}
			if($op=='display'){
				$active = 2;
				// 该置业的所有客户
				$customer = pdo_fetchall("select * from ". tablename('broke_customer'). "where cid =".$id. " and weid =". $_W['uniacid']. " $protectwhere ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
				$total = pdo_fetchcolumn("select count(id) from ". tablename('broke_customer'). "where cid =".$id. " and weid =". $_W['uniacid']." $protectwhere ");
			}
			$pager = pagination1($total, $pindex, $psize);
			
			$loupan = pdo_fetchall('SELECT id, title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` = 1 ORDER BY displayorder DESC",array(':weid' => $_W['uniacid']));
			$pan = array();
			foreach($loupan as $l){
				$pan[$l['id']] = $l['title'];
			}
			
			
			// 该置业的客户详情
			if($op=='detail'){
				$cid=$_GPC['cid'];
				if(intval($cid)){
					$customer = pdo_fetch('SELECT * FROM '.tablename('broke_customer')." WHERE `weid` = :weid AND id=:cid LIMIT 1",array(':weid' => $_W['uniacid'],':cid'=>$cid));
					$comm = pdo_fetchcolumn("select sum(commission) as commission from". tablename('broke_commission'). "where flag != 2 and weid =". $_W['uniacid']. " and cid =".$cid);
					// 推荐人
					$member = pdo_fetch("SELECT mobile, realname FROM ".tablename('broke_member'). " WHERE `weid` = :weid AND `from_user` =:from_user LIMIT 1",array(':weid' => $_W['uniacid'],':from_user'=>$customer['from_user']));
					
				}else{
					message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
					exit;
				}
				$time_node='';
				$time_detail='';
				
				for($i=0; $i<=$statuslenth; $i++){ 
					if($customer['status']>2 && $i==2){
						continue;
					}
					if($customer['status']==2 && $i==2){
						$time_node .= '<i class="time-node"><input type="radio" name="status" value="'.$i.'"/></i>'; 
						$time_detail .='<li class="fn-clear"><div class="time-detail"><p class="time-event">'.$status[$i].'</p></div></li>';
						break;
					}
					if($i<=$customer['status']){
						if($i==$statuslenth){
							$time_node .= '<i class="time-node"><input type="radio" name="status" value="'.$i.'"/></i>'; 
						}else{
							$time_node .= '<i class="time-node"><input type="radio" name="status" value="'.$i.'"/></i><span class="time-line"></span>'; 
						}
						$time_detail .='<li class="fn-clear"><div class="time-detail"><p class="time-event">'.$status[$i].'</p></div></li>';
					}else{
						if($i==$statuslenth){
							$time_node .= '<i class="time-node-no"><input type="radio" name="status" value="'.$i.'"/></i>'; 
						}else{
							$time_node .= '<i class="time-node-no"><input type="radio" name="status" value="'.$i.'"/></i><span class="time-line-no"></span>'; 
						}
						$time_detail .='<li class="fn-clear"><div class="time-detail-no"><p class="time-event">'.$status[$i].'</p></div></li>';
					
					}
				}
				
				include $this->template('gw_customer');
				exit;
			}
			
			//改变客户状态
			if($op=='status'){
				$cid = $_GPC['cid'];
				$statuss = array(
					'status'=>$_GPC['status'],
					'content'=>$_GPC['content'],
					'updatetime'=>TIMESTAMP
				);
				$temp = pdo_update('broke_customer', $statuss, array('id'=>$_GPC['cid']));
				//推荐人ID
				$mid = pdo_fetchcolumn("select m.id from ". tablename('broke_member'). " as m left join". tablename('broke_customer'). " as c on m.from_user = c.from_user and m.weid = c.weid where m.weid =". $_W['uniacid']. " and c.id =".$_GPC['cid']);
				$cstatus = pdo_fetchall("select status from". tablename('broke_commission'). "where cid =". $cid. ".and mid =".$mid);
				$isupdate = 1;
				foreach($cstatus as $s){
					if($s['status']==$_GPC['status']){
						$isupdate = 0;
					}
				}
				if(!$mid){
					$mid = 0;
				}else{
					$tjmid = pdo_fetchcolumn("select tjmid from". tablename('broke_member'). "where id =". $mid);
					$tjmid = intval($tjmid);
				}
				if($isupdate){
					$commission = array(
						'weid'=>$_W['uniacid'],
						'mid'=>$mid,
						'cid'=>$cid,
						'commission'=>intval($_GPC['commission']),
						'content'=>$_GPC['content'],
						'status'=>$_GPC['status'],
						'flag'=>$_GPC['flag'],
						'opid'=>$assistant['id'],
						'opname'=>$assistant['opname'],
						'createtime'=>time()
					);
					$temp = pdo_insert('broke_commission', $commission);
					if($tjmid){
					
						$teamfy=intval($rule['teamfy']);
						
						if($teamfy){
							$commission_team = array(
								'weid'=>$_W['uniacid'],
								'mid'=>$tjmid,
								'cid'=>$cid,
								'commission'=>intval(intval($_GPC['commission'])*$teamfy/100),
								'content'=>$_GPC['content'],
								'status'=>$_GPC['status'],
								'flag'=>$_GPC['flag'],
								'opid'=>$assistant['id'],
								'opname'=>$assistant['opname'],
								'tid'=>$mid,
								'createtime'=>time()
							);
							$temp = pdo_insert('broke_commission', $commission_team);
						}	
					}
					
					
				}else{
					$commission = array(
						'commission'=>intval($_GPC['commission']),
						'content'=>$_GPC['content'],
						'opid'=>$assistant['id'],
						'opname'=>$assistant['opname'],
						'createtime'=>time()
					);
					$temp = pdo_update('broke_commission', $commission, array('cid'=>$cid, 'mid'=>$mid, 'status'=>$_GPC['status']));
					if($tjmid){
					
						$teamfy=pdo_fetchcolumn("select teamfy from". tablename('broke_rule'). "where weid =". $weid);
						$teamfy=intval($teamfy);
						if($teamfy){
							$commission_team = array(
								
								'commission'=>intval(intval($_GPC['commission'])*$teamfy/100),
								'content'=>$_GPC['content'],
								'opid'=>0,
								'opname'=>'',
								'createtime'=>time()
							);
							pdo_update('broke_commission', $commission_team, array('cid'=>$cid,'mid'=>$tjmid, 'status'=>$_GPC['status']));
						}	
					}
						
				}
				
				if($temp){
					echo 1;
				}else{
					echo 0;
				}
				exit;
			}
			
			include $this->template('gw_index');
			exit;
			
		}
		//插入
		if($op=='add'){
			
			$data=array(
				'weid'=>$_W['uniacid'],
				'from_user'=>$from_user,
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile'],
				'company'=>$_GPC['company'],
				'code'=>$_GPC['code'],
				'flag'=>0,
				'createtime'=>TIMESTAMP
				
			);
		
			$profile = pdo_fetch('SELECT code,id FROM '.tablename('broke_assistant')." WHERE flag = 0 and `weid` = :weid AND code=:code ",array(':weid' => $_W['uniacid'],':code' => $_GPC['code']));
			if($data['code']==$profile['code']){
				echo '-1';
				exit;
			}
			$codes = pdo_fetchall("select id, code from ". tablename('broke_counselor'). "where status = 1 and weid =".$_W['uniacid']);
			$flag = true;

			foreach($codes as $c){
				if(trim($c['code'])==trim($_GPC['code'])){
					pdo_update('broke_counselor', array('status'=>0), array('id'=>$c['id']));
					$flag = false;
					break;
				}
			}
			if($flag){
				echo -1;
				exit;
			}
			pdo_insert('broke_assistant',$data);
			
			echo 1;
			exit;
		}

		include $this->template('hlcounselor');
	}
	
	//案场经理
	public function doMobileAcmanager(){
		global $_W,$_GPC;
		
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		
		$weid = $_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user=$_W['openid'];	
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		
		$profile=pdo_fetch('SELECT a.*, ac.loupanid FROM '.tablename('broke_assistant')." as a left join ". tablename('broke_acmanager'). " as ac on a.weid = ac.weid and a.code = ac.code WHERE flag = 1 and a.weid = :weid  AND a.from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		if(empty($profile['loupanid'])){
			$profile['loupanid'] = 0;
		}
		$id = $profile['id'];
		if(intval($id) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		
		$status = $this->ProcessStatus();
		$statuslenth=count($status)-1;
		$cfg = $this->module['config'];
		$statusprotect=count($status)-2;
		$protectdate = intval($cfg['protectdate']);
		
		if($protectdate){
			$protectdate = TIMESTAMP - (3600*24*$protectdate);
			$protectwhere = " AND (updatetime > $protectdate OR status > $statusprotect )";

		}	
		if(intval($id)){
		
			if($op=='allot'){
				$op = 'allot';
				if($_GPC['id'] > 0){
					// 销售员ID
					$id = intval($_GPC['id']);
					$update = array(
						'cid'=>$id,
						'allottime'=>time()
					);
					// 所分配客户ID数组
					$selected = explode(',',trim($_GPC['selected']));
					for($i=0; $i<sizeof($selected); $i++){
						$temp = pdo_update('broke_customer', $update, array('id'=>$selected[$i]));
					}
					if(!$temp){
						message('分配失败，请重新分配！', $this->createMobileUrl('acmanager', array('op'=>'allot')), 'error');
					}else{
						message('分配成功！', $this->createMobileUrl('acmanager',array('op'=>'mycustomer','opp'=>'his', 'cid' => $id)), 'success');
					}
					
				}else{
					$selected = trim($_GPC['selected']);
				}
				//所有置业
				$customer = pdo_fetchall("select * from ". tablename('broke_assistant'). "where flag = 0 and status = 1 and weid =". $_W['uniacid']);
			
				include $this->template('ga_index');
				exit;
			}
			
			$opp = 'his';
			
			$pindex = max(1, intval($_GPC['page']));
			$psize = 15;
			//所有客户
			$total = pdo_fetchcolumn("select count(id) from ". tablename('broke_customer'). "where loupan in (". $profile['loupanid']. ") and weid =". $_W['uniacid'].$protectwhere );
			$customer = pdo_fetchall("select * from ". tablename('broke_customer'). "where loupan in (". $profile['loupanid']. ") and weid =". $_W['uniacid']. " $protectwhere ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			$pager = pagination1($total, $pindex, $psize);
			
			$loupan = pdo_fetchall('SELECT id, title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` = 1 ORDER BY displayorder DESC",array(':weid' => $_W['uniacid']));
			$pan = array();
			foreach($loupan as $l){
				$pan[$l['id']] = $l['title'];
			}
			
			include $this->template('gw_index');
			exit;
		}
		//插入
		if($op=='add'){
			
			$data=array(
				'weid'=>$_W['uniacid'],
				'from_user'=>$from_user,
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile'],
				'code'=>$_GPC['code'],
				'flag'=>1,
				'createtime'=>TIMESTAMP
				
			);
		
			$profile = pdo_fetch('SELECT code,id FROM '.tablename('broke_assistant')." WHERE flag = 1 and `weid` = :weid AND code=:code ",array(':weid' => $_W['uniacid'],':code' => $_GPC['code']));
			if($data['code']==$profile['code']){
				echo '-1';
				exit;
			}
			$codes = pdo_fetchall("select id, code from ". tablename('broke_acmanager'). "where status = 1 and weid =".$_W['uniacid']);
			$flag = true;

			foreach($codes as $c){
				if(trim($c['code'])==trim($_GPC['code'])){
					pdo_update('broke_acmanager', array('status'=>0), array('id'=>$c['id']));
					$flag = false;
					break;
				}
			}
			if($flag){
				echo -1;
				exit;
			}
			pdo_insert('broke_assistant',$data);
			
			echo 1;
			exit;
		}

		include $this->template('hlacmanager');
		
		
	}
	
	//注册
	public function doMobileRegister(){
		global $_W,$_GPC;
		
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$shareid = 'broke_shareid'.$_W['uniacid'];
		$from_user = $_W['openid'];	
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('关注后才能注册全民经纪人',$rule['gzurl'],'error');
			exit;
		}
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		//已存不让提交	
		$profile = pdo_fetchcolumn('SELECT count(*) FROM '.tablename('broke_member')." WHERE `weid` = :weid AND from_user=:from_user ",array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		if($profile){
			message('你已注册过啦！',$this->createMobileUrl('index'),'sucess');
			exit;
		}else{
			$isbroker = pdo_fetchall("SHOW TABLES LIKE 'ims_fxdaren_member'");
			if($isbroker){
				$broker = pdo_fetch(" SELECT * FROM ".tablename('fxdaren_member')." WHERE from_user='".$from_user."' AND weid=".$_W['uniacid']." ");
				if($broker){
					$member = array(
						'weid' => $_W['uniacid'],
						'from_user' => $from_user,
						'realname' => $broker['realname'],
						'mobile' => $broker['mobile'],
						'createtime' => TIMESTAMP,
					);
					$temp = pdo_insert('broke_member',$member);
					if($temp == false){
						message('注册失败，请返回重试！');
					}else{
						message('注册成功！', $this->createMobileUrl('share'), 'success');
					}
				}
			}
		}
		
		if($op=='display'){
			$identity = pdo_fetchall('SELECT * FROM '.tablename('broke_identity')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));
			include $this->template('hlregister');
			exit;
		}
		
		//插入
		if($op=='add'){
			$data=array(
				'weid'=>$_W['uniacid'],
				'tjmid'=>$_COOKIE[$shareid],
				'from_user'=>$from_user,
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile'],
				'company'=>$_GPC['company'],
				'identity'=>intval($_GPC['identity']),
				'status'=>1,
				'createtime'=>TIMESTAMP
				
			);
			$profile = pdo_fetch('SELECT mobile,id FROM '.tablename('broke_member')." WHERE `weid` = :weid AND mobile=:mobile ",array(':weid' => $_W['uniacid'],':mobile' => $_GPC['mobile']));
			
			if($data['mobile']==$profile['mobile']){
				echo '-2';
				exit;
			}
			pdo_insert('broke_member',$data);
			setcookie("$shareid", '');
			echo 1;
			exit;
		}
	}
	
	//我要推荐
	public function doMobileRecommend(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user = $_W['openid'];
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		$shareid = 'broke_shareid'.$_W['uniacid'];	
		//自己登录
		if(empty($_COOKIE[$shareid])){
		
		}
		//转发
		else{
			$profile= pdo_fetchcolumn('SELECT id FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			//已注册
			if(!empty($profile)){
			
			}
			//未注册
			else{
				echo "<script>
						if(confirm('您是否要注册?')){
							window.location.href = '".$this->createMobileUrl('register', array('op'=>'display'))."';
						}else{
							window.location.href = '".$this->createMobileUrl('yuyue')."';
						}
							
					</script>";
			}
		}

		$profile= pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		if(intval($profile['id']) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		if(empty($profile)){
			message('请先注册',$this->createMobileUrl('register'),'error');
			exit;
		}
		if($op=='display'){
			
			$loupans = pdo_fetchall('SELECT * FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ORDER BY displayorder DESC",array(':weid' => $_W['uniacid']));
			include $this->template('hlrecommend');
		}
		
		//插入
		if($op=='add'){
			
			$data=array(
				'weid'=>$_W['uniacid'],
				'from_user'=>$from_user,
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile'],
				'loupan'=>intval($_GPC['loupan']),
				'createtime'=>TIMESTAMP,
				'updatetime'=>TIMESTAMP
				
			);
			$status = $this->ProcessStatus();
			$statuslenth=count($status)-1;
			$cfg = $this->module['config'];
			$statusprotect=count($status)-2;
			$protectdate = intval($cfg['protectdate']);
			
			if($protectdate){
					$protectdate = TIMESTAMP - (3600*24*$protectdate);
									$protectwhere = " AND (updatetime > $protectdate OR status > $statusprotect )";

			}
			$profile = pdo_fetchcolumn('SELECT count(id) FROM '.tablename('broke_customer')." WHERE mobile=:mobile AND `weid` = :weid $protectwhere ",array(':weid' => $_W['uniacid'],':mobile' => $_GPC['mobile']));

			$cprotect = pdo_fetchcolumn('SELECT count(id) FROM '.tablename('broke_protect')." WHERE `weid` = :weid AND mobile=:mobile ",array(':weid' => $_W['uniacid'],':mobile' => $_GPC['mobile']));
			
			
			
			if($profile || $cprotect){
				echo '-1';
				exit;
			}
			pdo_insert('broke_customer',$data);
			//可用于未来插入短信发送接口
			echo 1;
			exit;
		}
		
	}
	
	//我的客户
	public function doMobileCustomer(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user=$_W['openid'];	
		
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		
		$shareid = 'broke_shareid'.$_W['uniacid'];	
		//自己登录
		if(empty($_COOKIE[$shareid])){
		
		}
		//转发
		else{
			$profile= pdo_fetchcolumn('SELECT id FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			//已注册
			if(!empty($profile)){
			
			}
			//未注册
			else{
				echo "<script>
						if(confirm('您是否要注册?')){
							window.location.href = '".$this->createMobileUrl('register', array('op'=>'display'))."';
						}else{
							window.location.href = '".$this->createMobileUrl('yuyue')."';
						}
							
					</script>";
			}
		}
		
		$profile= pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		if(intval($profile['id']) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		if(empty($profile)){
			message('请先注册',$this->createMobileUrl('register'),'error');
			exit;
		}
		$loupans = pdo_fetchall('SELECT id,title,tel FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
		$pan=array();
		foreach($loupans as $k=>$v){
			$pan[$v['id']]=$v['title'];
			$tel[$v['id']]=$v['tel'];
		}
		$status = $this->ProcessStatus();
		$statuslenth=count($status)-1;
		$cfg = $this->module['config'];
		$statusprotect=count($status)-2;
		$protectdate = intval($cfg['protectdate']);
		
		if($protectdate){
			$protectdate = TIMESTAMP - (3600*24*$protectdate);
			$protectwhere = " AND (updatetime > $protectdate OR status > $statusprotect )";

		}	
		
		if($op=='display'){
			
			$pindex = max(1, intval($_GPC['page']));
			$psize = 15;
			$total = pdo_fetchcolumn('SELECT count(id) FROM '.tablename('broke_customer')." WHERE `weid` = :weid AND `from_user` =:from_user $protectwhere ORDER BY id DESC ",array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			$pager = pagination1($total, $pindex, $psize);
			$customer = pdo_fetchall('SELECT * FROM '.tablename('broke_customer')." WHERE `weid` = :weid AND `from_user` =:from_user $protectwhere ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize,array(':weid' => $_W['uniacid'],':from_user' => $from_user));

			//echo $pager;
			include $this->template('hlcustomer');
			exit;
		}
		
		if($op=='detail'){
			$cid=$_GET['cid'];
			if(intval($cid)){
				$customer = pdo_fetch('SELECT * FROM '.tablename('broke_customer')." WHERE `weid` = :weid AND `from_user` =:from_user AND id=:cid LIMIT 1",array(':weid' => $_W['uniacid'],':cid'=>$cid,':from_user'=>$from_user));
				
			}else{
				message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
				exit;
			}
			$time_node='';
			$time_detail='';
			
			for($i=0; $i<=$statuslenth; $i++){ 
					if($customer['status']>2 && $i==2){
						continue;
					}
					if($customer['status']==2 && $i==2){
						$time_node .= '<i class="time-node"></i>'; 
						$time_detail .='<li class="fn-clear"><div class="time-detail"><p class="time-event">'.$status[$i].'</p></div></li>';
						break;
					}
					if($i<=$customer['status']){
						if($i==$statuslenth){
							$time_node .= '<i class="time-node"></i>'; 
						}else{
							$time_node .= '<i class="time-node"></i><span class="time-line"></span>'; 
						}
						$time_detail .='<li class="fn-clear"><div class="time-detail"><p class="time-event">'.$status[$i].'</p></div></li>';
					}else{
						if($i==$statuslenth){
							$time_node .= '<i class="time-node-no"></i>'; 
						}else{
							$time_node .= '<i class="time-node-no"></i><span class="time-line-no"></span>'; 
						}
						$time_detail .='<li class="fn-clear"><div class="time-detail-no"><p class="time-event">'.$status[$i].'</p></div></li>';
					
					}
				}
			
			include $this->template('hlcustomershow');
			exit;
		}
		
	}
	
	//我的佣金
	public function doMobileCommission(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user = $_W['openid'];
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		
		$shareid = 'broke_shareid'.$_W['uniacid'];	
		//自己登录
		if(empty($_COOKIE[$shareid])){
		
		}
		//转发
		else{
			$profile= pdo_fetchcolumn('SELECT id FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			//已注册
			if(!empty($profile)){
			
			}
			//未注册
			else{
				echo "<script>
						if(confirm('您是否要注册?')){
							window.location.href = '".$this->createMobileUrl('register', array('op'=>'display'))."';
						}else{
							window.location.href = '".$this->createMobileUrl('yuyue')."';
						}
							
					</script>";
			}
		}
		
		$profile= pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		if(intval($profile['id']) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		if(empty($profile)){
			message('请先注册',$this->createMobileUrl('register'),'error');
			exit;
		}

		// 已结佣金
		$mycommission = pdo_fetchcolumn("select commission from". tablename('broke_member'). " where id =". $profile['id']);
		// 账户总佣金
		$commission = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). " where flag != 2 and mid =". $profile['id']. " and weid =". $_W['uniacid']);
		// 可结佣金
		$comm = $commission - $profile['commission'];
		// 账目明细
		if($op == 'more'){
			$op = 'more';
			$pindex = max(1, intval($_GPC['page']));
			$psize = 10;
			$list = pdo_fetchall("select co.*, cu.realname from ". tablename('broke_commission'). " as co left join ".tablename('broke_customer')." as cu on co.cid = cu.id and co.weid = cu.weid where co.mid =". $profile['id']. " and co.flag != 2 ORDER BY co.createtime DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			$total = pdo_fetchcolumn("select count(id) from ". tablename('broke_commission'). " where mid =". $profile['id']. " and flag != 2");
			$pager = pagination1($total, $pindex, $psize);
		}else{
			$list = pdo_fetchall("select co.*, cu.realname from ". tablename('broke_commission'). " as co left join ".tablename('broke_customer')." as cu on co.cid = cu.id and co.weid = cu.weid where co.mid =". $profile['id']. " and co.flag != 2 ORDER BY co.createtime DESC limit 10");
			$total = pdo_fetchcolumn("select count(id) from ". tablename('broke_commission'). " where mid =". $profile['id']. " and flag != 2");
		}
		
		//$mycommission=number_format($mycommission, 2);

		include $this->template('hlcommission');
		
	}
	
	//我的团队
	public function doMobileTerm(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user = $_W['openid'];
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		$shareid = 'broke_shareid'.$_W['uniacid'];	
		//自己登录
		if(empty($_COOKIE[$shareid])){
		
		}
		//转发
		else{
			$profile= pdo_fetchcolumn('SELECT id FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			//已注册
			if(!empty($profile)){
			
			}
			//未注册
			else{
				echo "<script>
						if(confirm('您是否要注册?')){
							window.location.href = '".$this->createMobileUrl('register', array('op'=>'display'))."';
						}else{
							window.location.href = '".$this->createMobileUrl('yuyue')."';
						}
							
					</script>";
			}
		}
		$profile=pdo_fetch('SELECT id, status FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		$id = $profile['id'];
		if(intval($id) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		if(empty($profile)){
			message('请先注册',$this->createMobileUrl('register'),'error');
			exit;
		}
		$pindex = max(1, intval($_GPC['page']));
		$psize = 15;
		$term = pdo_fetchall("select realname, mobile from ". tablename('broke_member'). " where tjmid =". $id." ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
		$total = pdo_fetchcolumn("select count(id) from ". tablename('broke_member'). "where tjmid =".$id );
		
		$pager = pagination1($total, $pindex, $psize);
		include $this->template('term');
		
	}
	
	//活动细则
	public function doMobileRule(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		
		$rule = pdo_fetchcolumn('SELECT rule FROM '.tablename('broke_rule')." WHERE  weid = :weid" , array(':weid' => $_W['uniacid']));

		include $this->template('hlrule');
		
	}

	//楼盘显示
	public function doMobileLoupan(){
		global $_W,$_GPC;
		
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$lid = $_GPC['lid'];
		$from_user = $_W['openid'];	
		if($op=='add'){
			$logloupan = array(
				'weid'=>$_W['uniacid'],
				'from_user'=>$from_user,
				'lid'=>$lid,
				'createtime'=>time(),
				'createtime1'=>date('Y-m-d', time())
			);
			pdo_insert('broke_logloupan', $logloupan);
			message(1,'','ajax');
		}
		
		if(empty($lid)){
			message('抱歉，产品不存在或者已删除！','','error');
		}
		
		$loupan = pdo_fetch("SELECT * FROM ".tablename('broke_loupan')." WHERE id = :id", array(':id' => $lid));
		if (empty($loupan)) {
			message('产品不存在或是已经被删除！');
		}
		if (!preg_match("/^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"])*$/", $loupan['music'])){
			$loupan['music'] = $_W['attachurl'] . $loupan['music'];
		}
		$result['list'] = pdo_fetchall("SELECT * FROM ".tablename('broke_photo')." WHERE lpid = :lpid ORDER BY displayorder DESC", array(':lpid' => $loupan['id']));
		foreach ($result['list'] as &$photo) {
			$photo['items'] = pdo_fetchall("SELECT * FROM ".tablename('broke_item')." WHERE photoid = :photoid", array(':photoid' => $photo['id']));
		}
		
		
		
		if(!empty($from_user)){
			//$profile = pdo_fetch("select * from ".tablename('mc_mapping_fans')." where uniacid = ".$_W['uniacid']." and openid = '".$from_user."'");
			$myheadimg = pdo_fetchcolumn("select avatar from ".tablename('mc_members')." where uid = ".$_W['member']['uid']);
			if(empty($myheadimg)){
				$myheadimg=$_W['siteroot'].'../addons/broke/style/images/header.png';
			}
			$profile = pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			$id = $profile['id'];
		}
		
		if(empty($loupan)){
			message('抱歉，产品不存在或者已删除！','','error');
		}
		
		$shareids = 'broke_shareid'.$_W['uniacid'];
		
		if(intval($_GPC['id'])){
			$shareid=intval($_GPC['id']);
			setcookie("$shareids", "$shareid", TIMESTAMP+3600*24*30);
		}
		
		$share_from_user = '';
		if(!empty($shareid)){
			$share_from_user = pdo_fetchcolumn("select from_user from".tablename('broke_member')."where id =".$shareid);
		}
		
		
		$log = array(
			'weid'=>$_W['uniacid'],
			'from_user'=>$from_user,
			'share_from_user'=>$share_from_user,
			'loupan'=>$lid,
			'browser'=>$_SERVER['HTTP_USER_AGENT'],
			'ip'=>getip(),
			'createtime'=>time(),
			'createtime1'=>date('Y-m-d', time())
		);
		pdo_insert('broke_log', $log);
			
		include $this->template('hlloupan');
		
	}
	
	public function doMobileMy(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user = $_W['openid'];	
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('关注后才能查看哦',$rule['gzurl'],'error');
			exit;
		}
		//$profile = pdo_fetch("select * from ".tablename('mc_mapping_fans')." where uniacid = ".$_W['uniacid']." and openid = '".$from_user."'");
		$myheadimg = pdo_fetchcolumn("select avatar from ".tablename('mc_members')." where uid = ".$_W['member']['uid']);
		if(empty($myheadimg)){
			$myheadimg='../addons/broke/style/images/header.png';
		}
		$profile= pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		
		if(intval($profile['id']) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		
		//已注册
		if(!empty($profile)){
			
			}
			//未注册
		else{
				echo "<script>
						if(confirm('您是否要注册?')){
							window.location.href = '".$this->createMobileUrl('register', array('op'=>'display'))."';
						}else{
							window.location.href = '".$this->createMobileUrl('yuyue')."';
						}
							
					</script>";
			}
		
		//$mycustomer= pdo_fetchcolumn('SELECT count(*) FROM '.tablename('broke_customer')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		//$mycommission= pdo_fetchcolumn('SELECT sum(`commisson`) FROM '.tablename('broke_commisson')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		//$mycommission=1;
		include $this->template('hlmy');
		
	}
	
	public function doMobileBindbank(){
		global $_W,$_GPC;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		$urlcookie = "broke_url".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$url = $_W['siteroot'].'.'.$_SERVER['REQUEST_URI'];
			$url = substr($url, 0, strrpos($url, '&'));
			setcookie($urlcookie, $url, time()+3600*240);
			$this->CheckCookie();
		} else {
			if(!empty($_COOKIE[$urlcookie])){
				$url = $_COOKIE[$urlcookie];
				setcookie($urlcookie, '', time()+3600*240);
				header("location:$url");
			}
		}
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user=$_W['openid'];	
	
		$rule = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		if(empty($from_user)){
			message('你想知道怎么加入么?',$rule['gzurl'],'sucessr');
			exit;
		}
		
		$shareid = 'broke_shareid'.$_W['uniacid'];	
		//自己登录
		if(empty($_COOKIE[$shareid])){
		
		}
		//转发
		else{
			$profile= pdo_fetchcolumn('SELECT id FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
			//已注册
			if(!empty($profile)){
			
			}
			//未注册
			else{
				echo "<script>
						if(confirm('您是否要注册?')){
							window.location.href = '".$this->createMobileUrl('register', array('op'=>'display'))."';
						}else{
							window.location.href = '".$this->createMobileUrl('yuyue')."';
						}
							
					</script>";
			}
		}
		
		$profile= pdo_fetch('SELECT * FROM '.tablename('broke_member')." WHERE  weid = :weid  AND from_user = :from_user" , array(':weid' => $_W['uniacid'],':from_user' => $from_user));
		if(intval($profile['id']) && $profile['status']==0){
			include $this->template('forbidden');
			exit;
		}
		if(empty($profile)){
			message('请先注册',$this->createMobileUrl('register'),'error');
			exit;
		}
		if($op=='edit'){
		
			$data=array(
				
				'bankcard'=>$_GPC['bankcard'],
				'banktype'=>$_GPC['banktype'],
				
				
			);
			if(!empty($data['bankcard']) && !empty($data['banktype'])){
				pdo_update('broke_member',$data,array('from_user' => $from_user));
				
				echo 1;
				
			}else{
				echo 0;
			}
			
			exit;
		}

		include $this->template('hlbindbank');
		
	}
	
	//预约
	public function doMobileYuyue(){
		global $_W,$_GPC;
		
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		$from_user=$_W['openid'];	
		if($op=='add'){
			$shareid = 'broke_shareid'.$_W['uniacid'];
			$shareid = $_COOKIE[$shareid];
			if(intval($shareid)){
				$share_from_user = pdo_fetchcolumn("select from_user from".tablename('broke_member')."where id =".$shareid);
			}else{
				$share_from_user='';
			}
			$data=array(
				'weid'=>$_W['uniacid'],
				'from_user'=>$share_from_user,
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile'],
				'loupan'=>intval($_GPC['loupan']),
				'createtime'=>TIMESTAMP,
				'updatetime'=>TIMESTAMP,
				'flag'=>1
			);
			$profile = pdo_fetch('SELECT loupan,id FROM '.tablename('broke_customer')." WHERE `flag` = :flag AND `weid` = :weid AND loupan=:loupan AND `from_user` = :from_user",array(':flag' => 1, ':weid' => $_W['uniacid'],':loupan' => $_GPC['loupan'], ':from_user' => $_W['openid']));
			
			if($data['loupan']==$profile['loupan']){
				echo '-1';
				exit;
			}
			pdo_insert('broke_customer',$data);
			
			echo 1;
			exit;
		}
		//$id = $_GPC['id'];
		$loupans = pdo_fetchall('SELECT * FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ORDER BY displayorder DESC",array(':weid' => $_W['uniacid']));
		include $this->template('hlyuyue');
		
	}
	
	
//以下为后台管理＝＝＝＝＝＝＝＝＝＝＝＝＝＝
	
	//身份管理
	public function doWebIdentity(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($op == 'display') {
			$list = pdo_fetchall('SELECT * FROM '.tablename('broke_identity')." WHERE `weid` = :weid ORDER BY listorder DESC",array(':weid' => $_W['uniacid']));
			if(checksubmit('submit')) {
				foreach ($_GPC['listorder'] as $key => $val) {
					pdo_update('broke_identity', array('listorder' => intval($val)),array('id' => intval($key)));
				}
				message('更新身份排序成功！', $this->createWebUrl('identity', array('op'=>'display')), 'success');
			}
			include $this->template('web/identity_list');
		}
		
		if($op == 'post') {
			$id = intval($_GPC['id']);
			if($id > 0) {
				$theone = pdo_fetch('SELECT * FROM '.tablename('broke_identity')." WHERE  weid = :weid  AND id = :id" , array(':weid' => $_W['uniacid'],':id' => $id));
			} else {
				$theone = array('status' => 1,'listorder' => 0, 'iscompany'=> 0);
			}

			if (checksubmit('submit')) {
				$identity_name = trim($_GPC['identity_name']) ?  trim($_GPC['identity_name']) : message('请填写身份名称！');
				$listorder = intval($_GPC['listorder']);
				$status = intval($_GPC['status']);
				$iscompany = intval($_GPC['iscompany']);
				$insert = array(
					'weid' => $_W['uniacid'],
					'identity_name' => $identity_name,
					'iscompany' => $iscompany,
					'listorder' => $listorder,
					'status' => $status,
					'createtime' => TIMESTAMP
				);
				if(empty($id)) {
					pdo_insert('broke_identity', $insert);
					!pdo_insertid() ?	message('保存身份数据失败, 请稍后重试.','error') : '';
				} else {
					if(pdo_update('broke_identity', $insert,array('id' => $id)) === false){
						message('更新身份数据失败, 请稍后重试.','error');
					}
				}
				message('更新身份数据成功！', $this->createWebUrl('identity', array('op'=>'display')), 'success');
			}
			include $this->template('web/identity_post');
		}
		if($op == 'del') {
			$temp = pdo_delete('broke_identity',array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('删除数据失败！', $this->createWebUrl('identity', array('op'=>'display')), 'error');
			}else{
				message('删除数据成功！', $this->createWebUrl('identity', array('op'=>'display')), 'success');
			}
		}
		
		
	}
	
	//楼盘管理
	public function doWebLoupan(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		load()->func('tpl');
		$op = $_GPC['op']?$_GPC['op']:'display';
		$op = !empty($_GPC['op']) ? $_GPC['op'] : 'display';

		if ($op == 'create') {
			$id = intval($_GPC['id']);
			if (!empty($id)) {
				$item = pdo_fetch("SELECT * FROM ".tablename('broke_loupan')." WHERE id = :id" , array(':id' => $id));
				if (empty($item)) {
					message('抱歉，产品不存在或是已经删除！', '', 'error');
				}
			}
			if (checksubmit('fileupload-delete')) {
				$data = array();
				$data['thumb'] = '';
				pdo_update('broke_loupan', $data, array('id' => $id));
				message('封面删除成功！', '', 'success');
			}
			if (checksubmit('submit')) {
				if (empty($_GPC['title'])) {
					message('请输入产品名称！');
				}
				$data = array(
					'weid' => $_W['uniacid'],
					'title' => $_GPC['title'],
					'thumb' => $_GPC['thumb'],
					'music' => $_GPC['music'],
					'open' => $_GPC['open'],
					'ostyle' => $_GPC['ostyle'],
					'icon' => $_GPC['icon'],
					'share' => $_GPC['share'],
					'content' => $_GPC['content'],
					'tel' => $_GPC['tel'],
					'province' => $_GPC['resideprovince'],
					'city' => $_GPC['residecity'],
					'dist' => $_GPC['residedist'],
					'addr' => $_GPC['addr'],
					'lng' => $_GPC['lng'],
					'lat' => $_GPC['lat'],
					'jw_addr' => $_GPC['jw_addr'],
					'displayorder' => intval($_GPC['displayorder']),
					'commission' => $_GPC['commission'],
					'isloop' => intval($_GPC['isloop']),
					'isview' => intval($_GPC['isview']),
					'type' => intval($_GPC['type']),
					'createtime' => TIMESTAMP,
				);
				if ($_GPC['mset'][0]) {
					$data['mauto'] = 1;
				}else{
					$data['mauto'] = 0;
				}
				if ($_GPC['mset'][1]) {
					$data['mloop'] = 1;
				}else{
					$data['mloop'] = 0;
				}
				if (empty($id)) {
					pdo_insert('broke_loupan', $data);
				} else {
					unset($data['createtime']);
					pdo_update('broke_loupan', $data, array('id' => $id));
				}
				message('产品更新成功！', $this->createWebUrl('loupan', array('op' => 'display')), 'success');
			}
			include $this->template('web/loupan');
		} elseif ($op == 'display') {
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$condition = '';
			if (!empty($_GPC['keyword'])) {
				$condition .= " AND title LIKE '%{$_GPC['keyword']}%'";
			}

			$list = pdo_fetchall("SELECT * FROM ".tablename('broke_loupan')." WHERE weid = '{$_W['uniacid']}' $condition ORDER BY displayorder DESC, id DESC LIMIT ".($pindex - 1) * $psize.','.$psize);
			$total = pdo_fetchcolumn('SELECT COUNT(*) FROM ' . tablename('broke_loupan') . " WHERE weid = '{$_W['uniacid']}' $condition");
			$pager = pagination($total, $pindex, $psize);
			if (!empty($list)) {
				foreach ($list as &$row) {
					$row['total'] = pdo_fetchcolumn("SELECT COUNT(*) FROM ".tablename('broke_photo')." WHERE lpid = :lpid", array(':lpid' => $row['id']));
				}
			}
			include $this->template('web/loupan');
		} elseif ($op == 'photo') {
			$id = intval($_GPC['lpid']);
			$loupan = pdo_fetch("SELECT id, type FROM ".tablename('broke_loupan')." WHERE id = :id", array(':id' => $id));
			if (empty($loupan)) {
				message('产品不存在或是已经被删除！');
			}
			if (checksubmit('submit')) {
		if (!empty($_GPC['item'])) {
			if (!empty($_GPC['id'])) {
				$data = array(
					'weid' => $_W['uniacid'],
					'lpid' => intval($_GPC['lpid']),
					'photoid' => intval($_GPC['photoid']),
					'type' => $_GPC['type'],
					'item' => $_GPC['item'],
					'url' => $_GPC['url'],
					'x' => $_GPC['x'],
					'y' => $_GPC['y'],
					'animation' => $_GPC['animation'],
				);
				pdo_update('broke_item', $data, array('id' => $_GPC['id']));
			}else{
				$data = array(
					'weid' => $_W['uniacid'],
					'lpid' => intval($_GPC['lpid']),
					'photoid' => intval($_GPC['photoid']),
					'type' => $_GPC['type'],
					'item' => $_GPC['item'],
					'url' => $_GPC['url'],
					'x' => $_GPC['x'],
					'y' => $_GPC['y'],
					'animation' => $_GPC['animation'],
				);
				pdo_insert('broke_item', $data);
			}
		}
				if (!empty($_GPC['attachment-new'])) {
					foreach ($_GPC['attachment-new'] as $index => $row) {
						if (empty($row)) {
							continue;
						}
						$data = array(
							'weid' => $_W['uniacid'],
							'lpid' => intval($_GPC['lpid']),
							'title' => $_GPC['title-new'][$index],
							'url' => $_GPC['url-new'][$index],
							'attachment' => $_GPC['attachment-new'][$index],
							'displayorder' => $_GPC['displayorder-new'][$index],
						);
						pdo_insert('broke_photo', $data);
					}
				}
				if (!empty($_GPC['attachment'])) {
					foreach ($_GPC['attachment'] as $index => $row) {
						if (empty($row)) {
							continue;
						}
						$data = array(
							'weid' => $_W['uniacid'],
							'lpid' => intval($_GPC['lpid']),
							'title' => $_GPC['title'][$index],
							'url' => $_GPC['url'][$index],
							'attachment' => $_GPC['attachment'][$index],
							'displayorder' => $_GPC['displayorder'][$index],
						);
						pdo_update('broke_photo', $data, array('id' => $index));
					}
				}
				message('产品更新成功！', $this->createWebUrl('loupan', array('op' => 'photo', 'lpid' => $loupan['id'])));
			}
			$photos = pdo_fetchall("SELECT * FROM ".tablename('broke_photo')." WHERE lpid = :lpid ORDER BY displayorder DESC", array(':lpid' => $loupan['id']));
			foreach ($photos as &$photo1) {
				$photo1['items'] = pdo_fetchall("SELECT * FROM ".tablename('broke_item')." WHERE photoid = :photoid", array(':photoid' => $photo1['id']));
			}
			include $this->template('web/loupan');
		} elseif ($op == 'delete') {
			$type = $_GPC['type'];
			$id = intval($_GPC['id']);
			if ($type == 'photo') {
				if (!empty($id)) {
					$item = pdo_fetch("SELECT * FROM ".tablename('broke_photo')." WHERE id = :id", array(':id' => $id));
					if (empty($item)) {
						message('图片不存在或是已经被删除！');
					}
					pdo_delete('broke_photo', array('id' => $item['id']));
				} else {
					$item['attachment'] = $_GPC['attachment'];
				}
				//file_delete($item['attachment']);
			} elseif ($type == 'loupan') {
				$loupan = pdo_fetch("SELECT id, thumb FROM ".tablename('broke_loupan')." WHERE id = :id", array(':id' => $id));
				if (empty($loupan)) {
					message('产品不存在或是已经被删除！');
				}
				$photos = pdo_fetchall("SELECT id, attachment FROM ".tablename('broke_photo')." WHERE lpid = :lpid", array(':lpid' => $id));
				if (!empty($photos)) {
					foreach ($photos as $row) {
						//file_delete($row['attachment']);
					}
				}
				pdo_delete('broke_loupan', array('id' => $id));
				pdo_delete('broke_photo', array('lpid' => $id));
			}
			message('删除成功！', referer(), 'success');
		} elseif ($op == 'cover') {
			$id = intval($_GPC['lpid']);
			$attachment = $_GPC['thumb'];
			if (empty($attachment)) {
				message('抱歉，参数错误，请重试！', '', 'error');
			}
			$item = pdo_fetch("SELECT * FROM ".tablename('broke_loupan')." WHERE id = :id" , array(':id' => $id));
			if (empty($item)) {
				message('抱歉，产品不存在或是已经删除！', '', 'error');
			}
			pdo_update('loupan', array('thumb' => $attachment), array('id' => $id));
			message('设置封面成功！', '', 'success');
		}
		
		
	}
	
	//客户管理
	public function doWebCustomer(){
		global $_W,$_GPC;
		checklogin();
		$weid=$_W['uniacid'];
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($op=='sort'){
			if($_GPC['loupan']==''){
				$sort = array(
					'realname'=>$_GPC['realname'],
					'mobile'=>$_GPC['mobile']
				);
				$list = pdo_fetchall("select c.*, m.id as mid, m.realname as tjrrealname from". tablename('broke_customer'). "as c left join".tablename('broke_member'). "as m on c.from_user = m.from_user and c.weid = m.weid where c.weid =". $_W['uniacid'].".and c.realname like '%". $sort['realname']."%' and c.mobile like '%".$sort['mobile']."%' ORDER BY id DESC");
				$commission = pdo_fetchall('SELECT cid,sum(commission) as commission FROM '.tablename('broke_commission')." WHERE `weid` = :weid group by cid",array(':weid' => $_W['uniacid']));
				$commissions = array();
				foreach($commission as $k=>$v){
					$commissions[$v['cid']]=$v['commission'];
				}
				
			}else{
				$sort = array(
					'realname'=>$_GPC['realname'],
					'mobile'=>$_GPC['mobile']
				);
				$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
				$loupans=array();
				foreach($loupan as $k=>$v){
					$loupans[$v['title']]=$v['id'];
				}
				$loupan = $loupans[$_GPC['loupan']];
				$pindex = max(1, intval($_GPC['page']));
				$psize = 20;
				$total = pdo_fetchcolumn("select count(c.id) from". tablename('broke_customer'). "as c left join".tablename('broke_member'). "as m on c.from_user = m.from_user and c.weid = m.weid where c.weid =". $_W['uniacid'].".and c.realname like '%". $sort['realname']."%' and c.mobile like '%".$sort['mobile']."%' and c.loupan =".$loupan);
				$pager = pagination($total, $pindex, $psize);
				$list = pdo_fetchall("select c.*, m.id as mid, m.realname as tjrrealname from". tablename('broke_customer'). "as c left join".tablename('broke_member'). "as m on c.from_user = m.from_user and c.weid = m.weid where c.weid =". $_W['uniacid'].".and c.realname like '%". $sort['realname']."%' and c.mobile like '%".$sort['mobile']."%' and c.loupan =".$loupan." ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
				
				$commission = pdo_fetchall('SELECT cid,sum(commission) as commission FROM '.tablename('broke_commission')." WHERE `weid` = :weid group by cid",array(':weid' => $_W['uniacid']));
				$commissions = array();
				foreach($commission as $k=>$v){
					$commissions[$v['cid']]=$v['commission'];
				}
			}
			$total = sizeof($list);
		}else{
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(c.id) from". tablename('broke_customer'). "as c left join".tablename('broke_member'). "as m on c.from_user = m.from_user and c.weid = m.weid where c.weid =". $_W['uniacid']);
			$pager = pagination($total, $pindex, $psize);
			//所有客户
			$list = pdo_fetchall("select c.*, m.id as mid, m.realname as tjrrealname from". tablename('broke_customer'). "as c left join".tablename('broke_member'). "as m on c.from_user = m.from_user and c.weid = m.weid where c.weid =". $_W['uniacid']. " ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			
			$commission = pdo_fetchall('SELECT cid,sum(commission) as commission FROM '.tablename('broke_commission')." WHERE `weid` = :weid group by cid",array(':weid' => $_W['uniacid']));
			$commissions = array();
			foreach($commission as $k=>$v){
				$commissions[$v['cid']]=$v['commission'];
			}
		}
		
		//$total = sizeof($list);
		
		if($op=='mycustomer'){
			// 置业的客户
			if($_GPC['opp']=='his'){
				$cid = $_GPC['cid'];
				$opp = 'his';
				$info = pdo_fetch("select id, weid, realname from". tablename('broke_assistant'). "where id =". $cid);
				$sort = array(
					'realname'=>$_GPC['realname'],
					'mobile'=>$_GPC['mobile']
				);
				//我的客户
				$list = pdo_fetchall("select c.*, m.realname as tjrrealname from". tablename('broke_customer'). " as c left join ".tablename('broke_member')." as m on c.from_user = m.from_user and c.weid = m.weid where c.cid ='". $cid. "' and c.weid =". $info['weid']. ".and c.realname like '%".$sort['realname']."%' and c.mobile like '%".$sort['mobile']."%'");
				$total = sizeof($list);
				$pager = '';
				
			}else{ // 经纪人的客户
				$id = $_GPC['id'];
				$info = pdo_fetch("select id, from_user, weid, realname from". tablename('broke_member'). "where id =". $_GPC['id']);
				$sort = array(
					'realname'=>$_GPC['realname'],
					'mobile'=>$_GPC['mobile']
				);
				//我的客户
				$list = pdo_fetchall("select c.*, m.realname as tjrrealname from". tablename('broke_customer'). " as c left join ".tablename('broke_member')." as m on c.from_user = m.from_user and c.weid = m.weid where c.from_user ='". $info['from_user']. "' and c.weid =". $info['weid']. ".and c.realname like '%".$sort['realname']."%' and c.mobile like '%".$sort['mobile']."%'");
				$total = sizeof($list);
				$pager = '';
			}
		}
		
		if($op=='cancel'){
			$cid = pdo_fetchcolumn("select cid from". tablename('broke_customer'). "where id =".$_GPC['id']);
			$cancel = array(
				'cid'=>0
			);
			$temp = pdo_update('broke_customer', $cancel, array('id'=>$_GPC['id']));
			if(!$temp){
				message('取消失败，请重新取消！', $this->createWebUrl('customer',array('op'=>'mycustomer','opp'=>'his', 'cid' => $cid)), 'error');
			}else{
				message('取消成功！', $this->createWebUrl('customer',array('op'=>'mycustomer','opp'=>'his', 'cid' => $cid)), 'success');
			}
			
		}
		
		//设置客户状态，推荐->跟进->无意向->到访->认筹->认购->签约->回款
		if($op=='status'){
		
			$ccid = $_GPC['cid'];
			$loupan = pdo_fetchcolumn("select loupan from". tablename('broke_customer'). "where id =". $_GPC['id']);
			
			$cid = $_GPC['id'];
			$mid = $_GPC['mid'];
			
			if(empty($mid)){
				$mid = 0;
			}else{
				$tjmid = pdo_fetchcolumn("select tjmid from". tablename('broke_member'). "where id =". $mid);
				$tjmid = intval($tjmid);
			}
			$cstatus = pdo_fetchall("select status from". tablename('broke_commission'). "where cid =". $cid. ".and weid =".$_W['uniacid']);
			$isupdate = 1;
			foreach($cstatus as $s){
				if($s['status']==$_GPC['status']){
					$isupdate = 0;
				}
			}
			if($isupdate){
				$commission = array(
					'weid'=>$_W['uniacid'],
					'mid'=>$mid,
					'cid'=>$cid,
					'commission'=>intval($_GPC['commission']),
					'content'=>$_GPC['content'],
					'status'=>$_GPC['status'],
					'flag'=>$_GPC['flag'],
					'createtime'=>time()
				);
				$temp = pdo_insert('broke_commission', $commission);
				
				if($tjmid){
					
					$teamfy=pdo_fetchcolumn("select teamfy from". tablename('broke_rule'). "where weid =". $weid);
					$teamfy=intval($teamfy);
					if($teamfy){
						$commission_team = array(
							'weid'=>$_W['uniacid'],
							'mid'=>$tjmid,
							'cid'=>$cid,
							'commission'=>intval(intval($_GPC['commission'])*$teamfy/100),
							'content'=>$_GPC['content'],
							'status'=>$_GPC['status'],
							'flag'=>$_GPC['flag'],
							'tid'=>$mid,
							'createtime'=>time()
						);
						$temp = pdo_insert('broke_commission', $commission_team);
					}	
				}
			}else{
				$commission = array(
					'commission'=>intval($_GPC['commission']),
					'content'=>$_GPC['content'],
					'opid'=>0,
					'opname'=>'',
					'createtime'=>time()
				);
				pdo_update('broke_commission', $commission, array('cid'=>$cid,'mid'=>$mid, 'status'=>$_GPC['status']));
				
				if($tjmid){
					
					$teamfy=pdo_fetchcolumn("select teamfy from". tablename('broke_rule'). "where weid =". $weid);
					$teamfy=intval($teamfy);
					if($teamfy){
						$commission_team = array(
							
							'commission'=>intval(intval($_GPC['commission'])*$teamfy/100),
							'content'=>$_GPC['content'],
							'opid'=>0,
							'opname'=>'',
							'createtime'=>time()
						);
						pdo_update('broke_commission', $commission_team, array('cid'=>$cid,'mid'=>$tjmid, 'status'=>$_GPC['status']));
					
					}	
				}
				
			}
			
			$status = array(
				'status'=>$_GPC['status'],
				'updatetime'=>time()
			);
			$temp = pdo_update('broke_customer', $status, array('id'=>$_GPC['id']));
			if(!empty($ccid)){
				if(empty($temp)){
					message('提交失败，请重新提交！', $this->createWebUrl('customer',array('op'=>'showdetail', 'opp'=>'showdetail', 'id' => $cid, 'cid'=>$ccid)), 'error');
				}else{
					message('提交成功！', $this->createWebUrl('customer',array('op'=>'mycustomer','opp'=>'his', 'cid' => $ccid)), 'success');
				}

			}else{
				if(empty($temp)){
					message('提交失败，请重新提交！', $this->createWebUrl('customer', array('op'=>'showdetail', 'id'=>$_GPC['id'], 'mid'=>$_GPC['mid'])), 'error');
				}else{
					if($mid == 0 ){
						message('提交成功！', $this->createWebUrl('customer'), 'success');
					}else{
						message('提交成功！', $this->createWebUrl('customer', array('op'=>'mycustomer', 'id'=>$_GPC['mid'])), 'success');
					}
				}
			}
		}
		$status = $this->ProcessStatus();
		if($op=='showdetail'){
			if($_GPC['opp']=='showdetail'){
				// 置业顾问ID
				$cid = $_GPC['cid'];
				// 客户ID
				$id = $_GPC['id'];
				//经济人姓名
				$realname = pdo_fetchcolumn("select m.realname from". tablename('broke_customer'). "as c left join".tablename('broke_member')."as m on c.from_user = m.from_user and c.weid = m.weid where c.id =". $id);
				$mid = pdo_fetchcolumn("select m.id from". tablename('broke_customer'). "as c left join".tablename('broke_member')."as m on c.from_user = m.from_user and c.weid = m.weid where c.id =". $id);
				//推荐楼盘
				//$loupan = pdo_fetchcolumn("select loupan from". tablename('broke_customer'). "where id =". $_GPC['id']);
				//var_dump($loupan);
				
				//推荐总佣金
				$comm = pdo_fetchcolumn("select sum(commission) as commission from". tablename('broke_commission'). "where weid =". $_W['uniacid']. " and cid =". $_GPC['id']);
				//每步备注
				$content = pdo_fetchall("select content, status from". tablename('broke_commission'). "where cid =". $_GPC['id']);
				$contents = array();
				foreach($content as $k=>$v){
					$contents[$v['status']] = $v['content'];
				}
				//推荐可获佣金
				$commission = pdo_fetchcolumn("select l.commission from". tablename('broke_loupan'). "as l left join".tablename('broke_customer')."as c on l.id=c.loupan and l.weid = c.weid where c.id =". $_GPC['id']);
				//客户
				$user = pdo_fetch("select * from". tablename('broke_customer'). "where id =". $_GPC['id']);
			}else{
				$mid = $_GPC['mid'];
				// 客户ID
				$id = $_GPC['id'];
				//经济人姓名
				if(empty($mid)){
					$mid = 0;
				}else{
					$realname = pdo_fetchcolumn("select realname from". tablename('broke_member'). "where id =". $mid);
				}
				//推荐楼盘
				//$loupan = pdo_fetchcolumn("select loupan from". tablename('broke_customer'). "where id =". $_GPC['id']);
				//var_dump($loupan);
				
				//推荐总佣金
				$comm = pdo_fetchcolumn("select sum(commission) as commission from". tablename('broke_commission'). "where cid =". $_GPC['id']." AND tid=0 ");
				//每步备注
				$content = pdo_fetchall("select content, status from". tablename('broke_commission'). "where cid =". $_GPC['id']);
				$contents = array();
				foreach($content as $k=>$v){
					$contents[$v['status']]=$v['content'];
				}
				
				//推荐可获佣金
				$commission = pdo_fetchcolumn("select l.commission from". tablename('broke_loupan'). "as l left join".tablename('broke_customer')."as c on l.id=c.loupan and l.weid = c.weid where c.id =". $_GPC['id']);
				//客户
				$user = pdo_fetch("select * from". tablename('broke_customer'). " where id =". $_GPC['id']);
			}
			$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
			$loupans=array();
			foreach($loupan as $k=>$v){
				$loupans[$v['id']]=$v['title'];
			}
			
			//佣金明细
			$list = pdo_fetchall("select * from ". tablename('broke_commission'). "where flag != 2 and cid =".$id. " and weid =".$_W['uniacid']. " AND tid=0 order by status");
			
			include $this->template('web/customer_showdetail');
			exit;
		}
		
		if($op=='del'){
			if($_GPC['opp']=='delete'){
				$cid = pdo_fetchcolumn("select cid from". tablename('broke_customer'). "where id =".$_GPC['id']);
				$temp = pdo_delete('broke_customer', array('id'=>$_GPC['id']));
				if(empty($temp)){
					message('删除失败，请重新删除！', $this->createWebUrl('customer',array('op'=>'mycustomer','opp'=>'his', 'cid' => $cid)), 'error');
				}else{
					message('删除成功！', $this->createWebUrl('customer',array('op'=>'mycustomer','opp'=>'his', 'cid' => $cid)), 'success');
				}
			}else{
				$temp = pdo_delete('broke_customer', array('id'=>$_GPC['id']));
				if(empty($temp)){
					if(empty($_GPC['mid'])){
						message('删除失败，请重新删除！', $this->createWebUrl('customer'), 'error');
					}else{
						message('删除失败，请重新删除！', $this->createWebUrl('customer', array('op'=>'mycustomer', 'id'=>$_GPC['mid'])), 'error');
					}
				}else{
					if(empty($_GPC['mid'])){
						message('删除成功！', $this->createWebUrl('customer'), 'success');
					}else{
						message('删除成功！', $this->createWebUrl('customer', array('op'=>'mycustomer', 'id'=>$_GPC['mid'])), 'success');
					}
				}
			}
		}
		
		$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
		$loupans = array();
		foreach($loupan as $k=>$v){
			$loupans[$v['id']]=$v['title'];
		}
		
		include $this->template('web/customer_show');
		
		
	}
	
	//佣金管理
	public function doWebCommission(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($op == 'mycommission'){
			$id = $_GPC['id'];
			$user = pdo_fetch("select * from ".tablename('broke_member'). " where id =". $id);
			// 帐户总佣金
			$commission = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). " where flag != 2 and mid =". $id. " and weid =". $_W['uniacid']);
			// 可结佣金
			$comm = $commission - $user['commission'];
			// 充值记录
			$list = pdo_fetchall("select * from ". tablename('broke_commission'). " where mid =". $id. " and flag = 2");
			
			include $this->template('web/mycommission_showdetail');
			exit;
		}

		if($op == 'send'){
			$mid = $_GPC['mid'];
			if(intval($_GPC['commission'])){
				$com = intval($_GPC['commission']);
			}else{
				message('请输入合法数值！', '', 'error');
			}
			$user = pdo_fetch("select * from ".tablename('broke_member'). " where id =". $mid);
			// 帐户总佣金
			$commissionall = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). " where flag != 2 and mid =". $mid. " and weid =". $_W['uniacid']);
			// 可结佣金
			$commke = $commissionall - $user['commission'];
			if($com>$commke){
				message('结佣超出可结佣金！', '', 'error');
			}
			$send = array(
				'weid'=>$_W['uniacid'],
				'mid'=>$mid,
				'commission'=>$com,
				'flag'=>2,
				'content'=>trim($_GPC['content']),
				'createtime'=>time(),
			);
			
			$commission = pdo_fetchcolumn("select commission from ".tablename('broke_member'). " where id =". $mid);
			$comm = array(
				'commission'=>$com+$commission
			);
			$temp = pdo_insert('broke_commission', $send);
			if(empty($temp)){
				message('充值失败，请重新充值！', $this->createWebUrl('commission', array('op'=>'mycommission', 'id'=>$mid)), 'error');
			}else{
				pdo_update('broke_member', $comm, array('id'=>$mid));
				message('充值成功！', $this->createWebUrl('commission', array('op'=>'mycommission', 'id'=>$mid)), 'success');
			}
		}
		
		if($op == 'display'){
			//推荐总佣金
			$tjcommission = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). "where weid =". $_W['uniacid']. ".and flag = 0");
			$tjcommission = !empty($tjcommission)?$tjcommission:0;
			//预约总佣金
			$yycommission = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). "where weid =". $_W['uniacid']. ".and flag = 1");
			$yycommission = !empty($yycommission)?$yycommission:0;
			//已结佣金
			$yjcommission = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). "where weid =". $_W['uniacid']. ".and flag = 2");
			$yjcommission = !empty($yjcommission)?$yjcommission:0;			
		}
		

		include $this->template('web/commission_show');

		
		
	}
	
	//经济人管理
	public function doWebMember(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($_GPC['op']=='showdetail'){
			$user = pdo_fetch("select * from". tablename('broke_member'). "where id =". $_GPC['id']);
			$identity = pdo_fetchall('SELECT id,identity_name FROM '.tablename('broke_identity')." WHERE `weid` = :weid and `status` =:status ",array(':weid' => $_W['uniacid'], ':status'=>1));
			$identitys=array();
			foreach($identity as $k=>$v){
				$identitys[$v['id']]=$v['identity_name'];
			}
			//我的id, from_user, weid;
			$info = pdo_fetch("select id, from_user, weid from". tablename('broke_member'). "where id =". $_GPC['id']);
			//我的客户数量 
			$count = pdo_fetchcolumn("select count(id) from". tablename('broke_customer'). "where from_user ='". $info['from_user']. "' and weid =". $info['weid']);
			//我的总佣金
			//$commission = pdo_fetchcolumn("select sum(commission) from". tablename('broke_commission'). "where flag != 2 and mid ='". $info['id']. "' and weid =". $info['weid']);
			//$commission = !empty($commission)?$commission:0;
			include $this->template('web/member_showdetail');
			exit;
		}
		
		
		
		//设置用户权限，可用-禁用
		if($_GPC['op']=='status'){
			$status = array(
				'status'=>$_GPC['status']
			);
			$temp = pdo_update('broke_member', $status, array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('设置用户权限失败，请重新设置！', $this->createWebUrl('member', array('op'=>'showdetail', 'id'=>$_GPC['id'])), 'error');
			}else{
				message('设置用户权限成功！', $this->createWebUrl('member'), 'success');
			}
		}
		
		if($_GPC['op']=='del'){
			$temp = pdo_delete('broke_member', array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('删除失败，请重新删除！', $this->createWebUrl('member'), 'error');
			}else{
				message('删除成功！', $this->createWebUrl('member'), 'success');
			}
		}
		
		$identity = pdo_fetchall('SELECT id,identity_name FROM '.tablename('broke_identity')." WHERE `weid` = :weid and `status` =:status ",array(':weid' => $_W['uniacid'], ':status'=>1));
		$identitys=array();
		foreach($identity as $k=>$v){
			$identitys[$v['id']]=$v['identity_name'];
		}
		
		if($_GPC['op']=='showteam'){
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(id) from". tablename('broke_member'). "where tjmid=".$_GPC['id']." AND weid =".$_W['uniacid']);
			$pager = pagination($total, $pindex, $psize);
			$list = pdo_fetchall("select * from". tablename('broke_member'). "where tjmid=".$_GPC['id']." AND weid =".$_W['uniacid']." ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			$mids=array();
			foreach($list as $v){
				$mids[]=$v['id'];
			}
			
			if(count($mids)){
				$mids = implode(",", $mids);
				//待优化
				//echo $mids;
				$commission = pdo_fetchall('SELECT tid, sum(commission) as commission FROM '.tablename('broke_commission')." WHERE   `weid` = :weid AND tid in(".$mids.")  group by mid",array(':weid' => $_W['uniacid']));
				//var_dump($commission);
				$commissions = array();
				foreach($commission as $k=>$v){
					$commissions[$v['tid']]=$v['commission'];
				}
			}
			
			//$total = sizeof($list);
			include $this->template('web/member_showteam');
			exit;
		}
		
		if($_GPC['op']=='sort'){
			$sort = array(
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile']
			);
			$list = pdo_fetchall("select * from". tablename('broke_member')."where weid =".$_W['uniacid'].".and realname like '%".$sort['realname']. "%' and mobile like '%".$sort['mobile']. "%' ORDER BY id DESC");
			$total = sizeof($list);
			$commission = pdo_fetchall('SELECT mid, sum(commission) as commission FROM '.tablename('broke_commission')." WHERE flag = 2 and `weid` = :weid group by mid",array(':weid' => $_W['uniacid']));
			$commissions = array();
			foreach($commission as $k=>$v){
				$commissions[$v['mid']]=$v['commission'];
			}
		}else{
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(id) from". tablename('broke_member'). "where weid =".$_W['uniacid']);
			$pager = pagination($total, $pindex, $psize);
			$list = pdo_fetchall("select * from". tablename('broke_member'). "where weid =".$_W['uniacid']." ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			$commission = pdo_fetchall('SELECT mid, sum(commission) as commission FROM '.tablename('broke_commission')." WHERE flag = 2 and `weid` = :weid group by mid",array(':weid' => $_W['uniacid']));
			$commissions = array();
			foreach($commission as $k=>$v){
				$commissions[$v['mid']]=$v['commission'];
			}
			//$total = sizeof($list);
		}
		
		include $this->template('web/member_show');
		
		
	}
	
	//活动细则管理
	public function doWebRule(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		$id = intval($_GPC['id']);

		$theone = pdo_fetch('SELECT * FROM '.tablename('broke_rule')." WHERE  weid = :weid" , array(':weid' => $_W['uniacid']));
		if(empty($theone)) {
			$theone['status']='到访,认筹,认购,签约,回款';
		}
		if (checksubmit('submit')) {
			$teamfy=intval($_GPC['teamfy']);
			$insert = array(
				'weid' => $_W['uniacid'],
				'rule' => htmlspecialchars_decode($_GPC['rule']),
				'terms' => htmlspecialchars_decode($_GPC['terms']),
				'status' => trim($_GPC['status']),
				'gzurl' => trim($_GPC['gzurl']),
				'teamfy' => $teamfy,
				'createtime' => TIMESTAMP
			);
			if(empty($id)) {
				if(empty($insert['status'])) {
					$insert['status']='到访,认筹,认购,签约,回款';
				
				}
				pdo_insert('broke_rule', $insert);
				!pdo_insertid() ?	message('保存失败, 请稍后重试.','error') : '';
			} else {
				if(pdo_update('broke_rule', $insert,array('id' => $id)) === false){
					message('更新失败, 请稍后重试.','error');
				}
			}
			message('更新成功！', $this->createWebUrl('rule'), 'success');
		}
		include $this->template('web/rule');
		
		
	}
	
	//数据分析
	public function doWebStat(){
		global $_W,$_GPC;
		$weid = $_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		//某年，默认为今年
		$year = !empty($_GPC['year'])?$_GPC['year']:date('Y');
		//某月，默认为当月
		$month = !empty($_GPC['month'])?$_GPC['month']:date('m');
		//所选年月的第一天
		$selecttime = $year.'-'.$month.'-01 00:00:00';
		//所选年月的第一天的时间戳
		$starttime = strtotime($selecttime); 
		$temptime = $selecttime;
		//当月最后一天的时间戳
		$endtime = strtotime(date('Y-m-d 23:59:59', strtotime("$temptime +1 month -1 day")));
		
		$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
		$loupans=array();
		foreach($loupan as $k=>$v){
			$loupans[$v['title']]=$v['id'];
		}
		//产品ID
		$lid = $loupans[$_GPC['loupan']];
		//记录搜索条件
		$condition = array(
			'op'=>$op,
			'year'=>$year,
			'month'=>$month,
			'loupan'=>$_GPC['loupan']
		);
		$pp=$_GPC['pp'];
		if($op=='customer'){
			$logtype = 1;
			
			if($pp=='xml'){
				//载入XML文件
				$xml = simplexml_load_file('../addons/broke/style/graph/customer.xml');
				if(empty($_GPC['loupan'])){
					//子标题
					$xml['subCaption'] = $year.'-'.$month;
					//当月的客户状态记录
					$logs = pdo_fetchall("select count(id) as num, status from".tablename('broke_customer')."where flag = 0 and weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by status");
				}else{
					//子标题
					$xml['subCaption'] = $year.'-'.$month.$_GPC['loupan'];
					//当月的该产品客户状态记录
					$logs = pdo_fetchall("select count(id) as num, status from".tablename('broke_customer')."where flag = 0 and loupan =".$lid.".and weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by status");
					//var_dump($logs);
				}
					
				foreach($logs as $key=>$log){
					$aa[$log['status']] = $log['num'];
				}

				$k = 0;		
				
				//设置客户状态记录
				foreach($xml as $a){
					if(!empty($aa[$k])){
						$a['value'] = $aa[$k];
					}else{
						$a['value'] = 0;
					}
					$k++;
				}
				
				header('Content-Type: text/xml');
				echo $xml->asXML();
				exit;
			}
			// $fp = fopen('../addons/broke/style/graph/customer.xml', 'w');
			// fwrite($fp, $xml->asXML());
			// fclose($fp);
		}
		
		if($op=='yuyue'){
			$logtype = 2;
			if($pp=='xml'){
				//载入XML文件
				$xml = simplexml_load_file('../addons/broke/style/graph/yuyue.xml');
				if(empty($_GPC['loupan'])){
					//子标题
					$xml['subCaption'] = $year.'-'.$month;
					//当月的客户状态记录
					$logs = pdo_fetchall("select count(id) as num, status from".tablename('broke_customer')."where flag = 1 and weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by status");
				}else{
					//子标题
					$xml['subCaption'] = $year.'-'.$month.$_GPC['loupan'];
					//当月的该产品客户状态记录
					$logs = pdo_fetchall("select count(id) as num, status from".tablename('broke_customer')."where flag = 1 and loupan =".$lid.".and weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by status");
					//var_dump($logs);
				}
					
				foreach($logs as $key=>$log){
					$aa[$log['status']] = $log['num'];
				}

				$k = 0;		
				
				//设置预约状态记录
				foreach($xml as $a){
					if(!empty($aa[$k])){
						$a['value'] = $aa[$k];
					}else{
						$a['value'] = 0;
					}
					$k++;
				}
					
				header('Content-Type: text/xml');
				echo $xml->asXML();
				exit;
			}
			// $fp = fopen('../addons/broke/style/graph/yuyue.xml', 'w');
			// fwrite($fp, $xml->asXML());
			// fclose($fp);
		}
		
		if($op=='display'){
			$logtype = 3;
			if($pp=='xml'){
				//载入XML文件
				$xml = simplexml_load_file('../addons/broke/style/graph/log.xml');
				if(empty($_GPC['loupan'])){
					//标题
					$xml['caption'] = $year.'-'.$month.' 游客浏览产品总记录曲线';
					//当月的所有浏览次数和访问人数记录
					$logs = pdo_fetchall("select count(id) as num,createtime1, count(distinct from_user) as num1 from".tablename('broke_log')."where weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by createtime1");
				}else{
					//标题
					$xml['caption'] = $year.'-'.$month.' 游客浏览'.$_GPC['loupan'].'记录曲线';
					//当月的该产品所有浏览次数和访问人数记录
					$logs = pdo_fetchall("select count(id) as num,createtime1, count(distinct from_user) as num1 from".tablename('broke_log')."where loupan =".$lid.".and weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by createtime1");
					//var_dump($logs);
				}
			
				//日期数组
				$dates = array();
				$n = 0;
				for($m=1; $m<=31; $m++){
					if($m<10){
						$n = '0'.$m;
					}else{
						$n = $m;
					}
					$dates[$n] = $year.'-'.$month.'-'.$n;
				}
				
				foreach($logs as $log){
					$key = substr($log['createtime1'], -2);
					if($log['createtime1']==$dates[$key]){
						$aa[$key] = $log['num'];
						$bb[$key] = $log['num1'];
					}else{
						$aa[$key] = 0;
						$bb[$key] = 0;
					}
				}

				$i = 1;
				$k = 0;		
				
				//设置浏览次数
				foreach($xml->dataset[0]->set as $a){
					if($i<10){
						$k = '0'.$i;
					}else{
						$k = $i;
					}
					if(!empty($aa[$k])){
						$a['value'] = $aa[$k];
					}else{
						$a['value'] = 0;
					}
					$i++;
				}
				
				$j = 1;
				$l = 0;
				//设置访问人数
				foreach($xml->dataset[1]->set as $a){
					if($j<10){
						$l = '0'.$j;
					}else{
						$l = $j;
					}
					if(!empty($bb[$l])){
						$a['value'] = $bb[$l];
					}else{
						$a['value'] = 0;
					}
					$j++;
				}
				
				header('Content-Type: text/xml');
				echo $xml->asXML();
				exit;
			}
			/*
			$fp = fopen('../addons/broke/style/graph/log.xml', 'w');
			fwrite($fp, $xml->asXML());
			fclose($fp);
			*/
		}
		
		if($op=='logloupan'){
			$logtype = 4;
			if($pp=='xml'){
				//载入XML文件
				$xml = simplexml_load_file('../addons/broke/style/graph/logloupan.xml');
				if(empty($_GPC['loupan'])){
					//标题
					$xml['caption'] = $year.'-'.$month.' 转发产品总记录曲线';
					$xml['subcaption'] = '';
					//当月的所有转发记录
					$logs = pdo_fetchall("select count(id) as num,createtime1 from".tablename('broke_logloupan')."where weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by createtime1");
				}else{
					//标题
					$xml['caption'] = $year.'-'.$month.' 转发'.$_GPC['loupan'].'记录曲线';
					$xml['subcaption'] = '';
					//当月的该产品的所有转发记录
					$logs = pdo_fetchall("select count(id) as num,createtime1 from".tablename('broke_logloupan')."where lid =".$lid.".and weid =".$_W['uniacid'].".and createtime >".$starttime.".and createtime <".$endtime.".group by createtime1");
					//var_dump($logs);
				}
				
				//日期数组
				$dates = array();
				$n = 0;
				for($m=1; $m<=31; $m++){
					if($m<10){
						$n = '0'.$m;
					}else{
						$n = $m;
					}
					$dates[$n] = $year.'-'.$month.'-'.$n;
				}
				
				foreach($logs as $log){
					$key = substr($log['createtime1'], -2);
					if($log['createtime1']==$dates[$key]){
						$aa[$key] = $log['num'];
					}else{
						$aa[$key] = 0;
					}
				}

				$i = 1;
				$k = 0;		
				
				//设置转发次数
				foreach($xml as $a){
					if($i<10){
						$k = '0'.$i;
					}else{
						$k = $i;
					}
					if(!empty($aa[$k])){
						$a['value'] = $aa[$k];
					}else{
						$a['value'] = 0;
					}
					$i++;
				}
					
				header('Content-Type: text/xml');
				echo $xml->asXML();
				exit;
				
				// $fp = fopen('../addons/broke/style/graph/logloupan.xml', 'w');
				// fwrite($fp, $xml->asXML());
				// fclose($fp);
			}
		}

		include $this->template('web/stat');
		
		
	}
	
	//置业顾问
	public function doWebCounselor(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($op == 'list') {
			$list = pdo_fetchall('SELECT * FROM '.tablename('broke_counselor')." WHERE `weid` = :weid ORDER BY listorder DESC",array(':weid' => $_W['uniacid']));
			if(checksubmit('submit')) {
				foreach ($_GPC['listorder'] as $key => $val) {
					pdo_update('broke_counselor', array('listorder' => intval($val)),array('id' => intval($key)));
				}
				message('更新销售员排序成功！', $this->createWebUrl('counselor', array('op'=>'list')), 'success');
			}
			include $this->template('web/counselor_list');
		}
		
		if($op == 'post') {
			$id = intval($_GPC['id']);
			if($id > 0) {
				$theone = pdo_fetch('SELECT * FROM '.tablename('broke_counselor')." WHERE  weid = :weid  AND id = :id" , array(':weid' => $_W['uniacid'],':id' => $id));
			} else {
				$theone = array('status' => 1,'listorder' => 0);
			}

			if (checksubmit('submit')) {
				$code = trim($_GPC['code']) ?  trim($_GPC['code']) : message('请填写邀请码！');
				$code1 = trim($_GPC['code1']);
				if($id > 0){
					if($code===$code1){
					
					}else{
						$codes = pdo_fetchall("select code from". tablename('broke_counselor'). "where weid =".$_W['uniacid']);
						foreach($codes as $c){
							if($code===$c['code']){
								message('已存在该邀请码请重新填写！');
							}
						}
					}
				}else{
					$codes = pdo_fetchall("select code from". tablename('broke_counselor'). "where weid =".$_W['uniacid']);
					foreach($codes as $c){
						if($code===$c['code']){
							message('已存在该邀请码请重新填写！');
						}
					}
				}
				$listorder = intval($_GPC['listorder']);
				$status = intval($_GPC['status']);
				$iscompany = intval($_GPC['iscompany']);
				$insert = array(
					'weid' => $_W['uniacid'],
					'code' => $code,
					'listorder' => $listorder,
					'status' => $status,
					'content' => trim($_GPC['content']),
					'createtime' => TIMESTAMP
				);
				if(empty($id)) {
					pdo_insert('broke_counselor', $insert);
					!pdo_insertid() ? message('保存销售员数据失败, 请稍后重试.','error') : '';
				} else {
					if(pdo_update('broke_counselor', $insert,array('id' => $id)) === false){
						message('更新销售员数据失败, 请稍后重试.','error');
					}
				}
				message('更新销售员数据成功！', $this->createWebUrl('counselor', array('op'=>'list')), 'success');
			}
			include $this->template('web/counselor_post');
		}
		
		if($op == 'del') {
			$temp = pdo_delete('broke_counselor',array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('删除数据失败！', $this->createWebUrl('counselor', array('op'=>'list')), 'error');
			}else{
				message('删除数据成功！', $this->createWebUrl('counselor', array('op'=>'list')), 'success');
			}
		}
		
		if($op == 'randomcode'){
			$num = trim($_GPC['num']) ?  trim($_GPC['num'])-2 : 7;
			$num = intval($num);
			$randomcode = 'ZY'.random($num, true);
			$code = pdo_fetchall("select code from". tablename('broke_counselor'). "where weid =".$_W['uniacid']);
			if(sizeof($code)>0){
				for($i=0; $i<sizeof($code); $i++){
					if($randomcode===$code[$i]['code']){
						$randomcode = 'ZY'.random($num, true);
						$i = -1;
					}
				}
			}
			message($randomcode,'','ajax');
		}

		if($op == 'counselorlist'){
			include $this->template('web/counselor_list');
		}
		
		
	}
	
	//案场经理
	public function doWebAcmanager(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($op == 'list') {
			$list = pdo_fetchall('SELECT * FROM '.tablename('broke_acmanager')." WHERE `weid` = :weid ORDER BY listorder DESC",array(':weid' => $_W['uniacid']));
			if(checksubmit('submit')) {
				foreach ($_GPC['listorder'] as $key => $val) {
					pdo_update('broke_acmanager', array('listorder' => intval($val)),array('id' => intval($key)));
				}
				message('更新经理排序成功！', $this->createWebUrl('acmanager', array('op'=>'list')), 'success');
			}
			include $this->template('web/acmanager_list');
		}
		
		if($op == 'post') {
			$id = intval($_GPC['id']);
			if($id > 0) {
				$theone = pdo_fetch('SELECT * FROM '.tablename('broke_acmanager')." WHERE  weid = :weid  AND id = :id" , array(':weid' => $_W['uniacid'],':id' => $id));
				// 将楼盘ID字苻串转为数组
				$theone['loupanid'] = explode(',', $theone['loupanid']);
			} else {
				$theone = array('status' => 1,'listorder' => 0);
			}

			if (checksubmit('submit')) {
				$code = trim($_GPC['code']) ?  trim($_GPC['code']) : message('请填写邀请码！');
				$code1 = trim($_GPC['code1']);
				if($id > 0){
					if($code===$code1){
					
					}else{
						$codes = pdo_fetchall("select code from". tablename('broke_acmanager'). "where weid =".$_W['uniacid']);
						foreach($codes as $c){
							if($code===$c['code']){
								message('已存在该邀请码请重新填写！');
							}
						}
					}
				}else{
					$codes = pdo_fetchall("select code from". tablename('broke_acmanager'). "where weid =".$_W['uniacid']);
					foreach($codes as $c){
						if($code===$c['code']){
							message('已存在该邀请码请重新填写！');
						}
					}
				}
				
				$listorder = intval($_GPC['listorder']);
				$status = intval($_GPC['status']);
				$loupanid = $_GPC['loupanid'];
				$loupanid = explode(',', $loupanid);
				foreach($loupanid as $key=>$l){
					if($l == null){
						unset($loupanid[$key]);
					}
				}
				$loupanid = implode(",", $loupanid);
				$insert = array(
					'weid' => $_W['uniacid'],
					'code' => $code,
					'listorder' => $listorder,
					'status' => $status,
					'content' => trim($_GPC['content']),
					'loupanid' => $loupanid,
					'createtime' => TIMESTAMP
				);
				if(empty($id)) {
					pdo_insert('broke_acmanager', $insert);
					!pdo_insertid() ? message('保存经理数据失败, 请稍后重试.','error') : '';
				} else {
					if(pdo_update('broke_acmanager', $insert,array('id' => $id)) === false){
						message('更新经理数据失败, 请稍后重试.','error');
					}
				}
				message('更新经理数据成功！', $this->createWebUrl('acmanager', array('op'=>'list')), 'success');
			}
			
			$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
			$loupans=array();
			foreach($loupan as $k=>$v){
				$loupans[$v['title']]=$v['id'];
			}
			
			include $this->template('web/acmanager_post');
		}
		
		if($op == 'del') {
			$temp = pdo_delete('broke_acmanager',array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('删除数据失败！', $this->createWebUrl('acmanager', array('op'=>'list')), 'error');
			}else{
				message('删除数据成功！', $this->createWebUrl('acmanager', array('op'=>'list')), 'success');
			}
		}
		
		if($op == 'randomcode'){
			$num = trim($_GPC['num']) ?  trim($_GPC['num'])-2 : 7;
			$num = intval($num);
			$randomcode = 'AC'.random($num, true);
			$code = pdo_fetchall("select code from". tablename('broke_acmanager'). "where weid =".$_W['uniacid']);
			if(sizeof($code)>0){
				for($i=0; $i<sizeof($code); $i++){
					if($randomcode===$code[$i]['code']){
						$randomcode = 'AC'.random($num, true);
						$i = -1;
					}
				}
			}
			message($randomcode,'','ajax');
		}

		if($op == 'acmanagerlist'){
			include $this->template('web/acmanager_list');
		}
		
		
	}
	
	//置业顾问管理
	public function doWebCounselors(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($op=='sort'){
			
			$sort = array(
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile']
			);
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(id) from". tablename('broke_assistant'). " where flag = 0 and weid =". $_W['uniacid'].".and realname like '%". $sort['realname']."%' and mobile like '%".$sort['mobile']."%'");
			$pager = pagination($total, $pindex, $psize);
			$list = pdo_fetchall("select * from". tablename('broke_assistant'). " where flag = 0 and weid =". $_W['uniacid'].".and realname like '%". $sort['realname']."%' and mobile like '%".$sort['mobile']."%' ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			
		}else{
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(id) from". tablename('broke_assistant'). " where flag = 0 and weid =". $_W['uniacid']);
			$pager = pagination($total, $pindex, $psize);
			//所有客户
			$list = pdo_fetchall("select * from". tablename('broke_assistant'). " where flag = 0 and weid =". $_W['uniacid']. " ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
		}

		//设置权限
		if($op=='status'){
			
			$counselors = array(
				'content'=>trim($_GPC['content']),
				'status'=>$_GPC['status']
			);
			$temp = pdo_update('broke_assistant', $counselors, array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('提交失败，请重新提交！', $this->createWebUrl('counselors', array('op'=>'showdetail', 'id'=>$_GPC['id'])), 'error');
			}else{
				message('提交成功！', $this->createWebUrl('counselors'), 'success');
			}
		}
		
		if($_GPC['op']=='showdetail'){
			$id = $_GPC['id'];
			//客户
			$user = pdo_fetch("select * from". tablename('broke_assistant'). "where id =". $_GPC['id']);

			include $this->template('web/counselors_showdetail');
			exit;
		}
		
		if($op=='del'){
			$code = pdo_fetchcolumn("select code from".tablename('broke_assistant'). "where id =".$_GPC['id']);
			$temp = pdo_delete('broke_assistant', array('id'=>$_GPC['id']));
			
			$c = pdo_fetchcolumn("select code from".tablename('broke_counselor'). "where weid =". $_W['uniacid']. " and code ='".$code. "'");
			if(!empty($c)){
				// 删除邀请码
				pdo_delete('broke_counselor', array('code'=>$c));
			}
			// 释放分配客户
			pdo_update('broke_customer', array('cid'=>0), array('cid'=>$_GPC['id']));
			
			if(empty($temp)){
				message('删除失败，请重新删除！', $this->createWebUrl('counselors', array('op'=>'showdetail', 'id'=>$_GPC['mid'])), 'error');
			}else{
				message('删除成功！', $this->createWebUrl('counselors'), 'success');
			}
		}
		
		if($op=='allot'){
			$op = 'allot';
			if($_GPC['id'] > 0){
				// 销售员ID
				$id = intval($_GPC['id']);
				$update = array(
					'cid'=>$id,
					'allottime'=>time()
				);
				// 所分配客户ID数组
				$selected = explode(',',trim($_GPC['selected']));
				for($i=0; $i<sizeof($selected); $i++){
					$temp = pdo_update('broke_customer', $update, array('id'=>$selected[$i]));
				}
				if(!$temp){
					message('分配失败，请重新分配！', $this->createWebUrl('customer'), 'error');
				}else{
					message('分配成功！', $this->createWebUrl('customer',array('op'=>'mycustomer','opp'=>'his', 'cid' => $id)), 'success');
				}
				
			}else{
				$selected = trim($_GPC['selected']);
			}
			
		}
		
		if($op=='mycustomer'){
			exit;
		}
		
		include $this->template('web/counselors_show');
		
		
	}
	
	//案场经理管理
	public function doWebAcmanagers(){
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$op = $_GPC['op']?$_GPC['op']:'display';
		if($_GPC['op']=='sort'){
		
			$sort = array(
				'realname'=>$_GPC['realname'],
				'mobile'=>$_GPC['mobile']
			);
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(id) from". tablename('broke_assistant'). " where flag = 1 and weid =". $_W['uniacid']." and realname like '%". $sort['realname']."%' and mobile like '%".$sort['mobile']."%'");
			$pager = pagination($total, $pindex, $psize);
			$list = pdo_fetchall("select * from". tablename('broke_assistant'). " where flag = 1 and weid =". $_W['uniacid']." and realname like '%". $sort['realname']."%' and mobile like '%".$sort['mobile']."%' ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
			
		}else{
			$pindex = max(1, intval($_GPC['page']));
			$psize = 20;
			$total = pdo_fetchcolumn("select count(id) from". tablename('broke_assistant'). " where flag = 1 and weid =". $_W['uniacid']);
			$pager = pagination($total, $pindex, $psize);
			//所有客户
			$list = pdo_fetchall("select * from". tablename('broke_assistant'). " where flag = 1 and weid =". $_W['uniacid']. " ORDER BY id DESC limit ".($pindex - 1) * $psize . ',' . $psize);
		}
		
		//设置权限
		if($_GPC['op']=='status'){
			
			$counselors = array(
				'content'=>trim($_GPC['content']),
				'status'=>$_GPC['status']
			);
			$temp = pdo_update('broke_assistant', $counselors, array('id'=>$_GPC['id']));
			if(empty($temp)){
				message('提交失败，请重新提交！', $this->createWebUrl('acmanagers', array('op'=>'showdetail', 'id'=>$_GPC['id'])), 'error');
			}else{
				message('提交成功！', $this->createWebUrl('acmanagers'), 'success');
			}
		}
		
		if($_GPC['op']=='showdetail'){
			$id = $_GPC['id'];
			//客户
			$user = pdo_fetch("select a.*, ac.loupanid, ac.id as codeid from". tablename('broke_assistant'). " as a left join ".tablename('broke_acmanager'). " as ac on a.weid = ac.weid and a.code = ac.code where a.id =". $_GPC['id']);
			$loupanids = explode(',', $user['loupanid']);
			$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid and `isview` =1 ",array(':weid' => $_W['uniacid']));
			$loupans=array();
			foreach($loupan as $k=>$v){
				$loupans[$v['id']]=$v['title'];
			}
			include $this->template('web/acmanagers_showdetail');
			exit;
		}
		
		if($_GPC['op']=='del'){
			$code = pdo_fetchcolumn("select code from".tablename('broke_assistant'). "where id =".$_GPC['id']);
			$temp = pdo_delete('broke_assistant', array('id'=>$_GPC['id']));
			// 删除邀请码
			$temp = pdo_delete('broke_acmanager', array('code'=>$code));
			
			if(empty($temp)){
				message('删除失败，请重新删除！', $this->createWebUrl('acmanagers', array('op'=>'showdetail', 'id'=>$_GPC['mid'])), 'error');
			}else{
				message('删除成功！', $this->createWebUrl('acmanagers'), 'success');
			}
		}
		
		
		include $this->template('web/acmanagers_show');
		
		
	}
	
	//产品元素
	public function doWebItem() {
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();		
		$lpid = intval($_GPC['lpid']);
		$photoid = intval($_GPC['photoid']);
		$id = intval($_GPC['id']);
		$photo = pdo_fetch("SELECT * FROM ".tablename('broke_photo')." WHERE id = :id", array(':id' => $photoid));
		if (empty($photo)) {
			message('场景不存在或是已经被删除！');
		}
		if (!empty($id)) {
			$item = pdo_fetch("SELECT * FROM ".tablename('broke_item')." WHERE id = :id", array(':id' => $id));
		}
		include $this->template('web/item');
		
		
	}
	
	//产品
	public function doWebQuery() {
		global $_W,$_GPC;
		$weid=$_W['uniacid'];
		checklogin();
		$kwd = $_GPC['keyword'];
		$sql = 'SELECT * FROM ' . tablename('broke_loupan') . ' WHERE `weid`=:weid AND `title` LIKE :title';
		$params = array();
		$params[':weid'] = $_W['uniacid'];
		$params[':title'] = "%{$kwd}%";
		$ds = pdo_fetchall($sql, $params);
		foreach($ds as &$row) {
			$r = array();
			$r['id'] = $row['id'];
			$r['title'] = $row['title'];
			$r['description'] = $row['content'];
			$r['thumb'] = $row['thumb'];
			$row['entry'] = $r;
		}
		include $this->template('web/query');
		
		
		
	}
	
	
	
	//导出
	public function doWebOutput() {
		global $_W,$_GPC;
		$loupan = pdo_fetchall('SELECT id,title FROM '.tablename('broke_loupan')." WHERE `weid` = :weid  ",array(':weid' => $_W['uniacid']),'id');
		$pstatus=$this->ProcessStatus();
		load()->func('tpl');		
		include $this->template('web/customer_output');
	}
	
	public function doWebdownload(){
		global $_W,$_GPC;
		checklogin();
		$weid = $_W['uniacid'];
		//每个页面都要用的公共信息.	
		$this->__web(__FUNCTION__);
	}
	
	public function doWebXlsupload() {
		global $_W,$_GPC;
		$op = $_GPC['op'];
		if($op=='import'){
			include_once("lib/reader.php");
			$tmp = $_FILES['file']['tmp_name'];
			$ext = end(explode('.', $_FILES['file']['name']));
			$ext = strtolower($ext);
			if($ext !='xls' || empty($tmp)){
				$str = '只支持xls文件！';
			}else{
				$sheets = array();
				$path = $_W['config']['upload']['attachdir'];
				if(!is_dir('../'.$path.'/xls')){
					mkdir('../'.$path.'/xls', 0777);
				}
				$save_path = $_W['config']['upload']['attachdir']."/xls/";
				//$save_path = '/'.$_W['config']['upload']['attachdir'];
				$file_name = '../'.$save_path.date('Ymdhis') . ".xls";
				//echo $file_name;
				if (copy($tmp, $file_name)) {
					$xls = new Spreadsheet_Excel_Reader();
					$xls->setOutputEncoding('utf-8');
					$xls->read($file_name);
					$createtime=TIMESTAMP;
					$numRows=$xls->sheets[0]['numRows'];
					$suscess_row=0;
					$error_row=0;
					$data=array(
						'createtime'=>$createtime,
						'weid'=>$_W['uniacid'],
					);
					for ($i=2; $i<=$numRows; $i++) {
						$sheets1 = $xls->sheets[0]['cells'][$i][1];
						$sheets2 = $xls->sheets[0]['cells'][$i][2];
						$data['cname']=$sheets1;
						$data['mobile']=$sheets2;
						
						if($i<12){
							$data_values .= " <tr><td>$i</td> <td> $sheets1</td> <td> $sheets2</td> </tr> ";
						}
						
						//开始插入
						if(pdo_insert('broke_protect',$data)){ 
							++$suscess_row; 
						}else{ 
							++$error_row; 
						}
					}
					
				  $str = $suscess_row." 条记录导入成功</br>";
				  $str .= $error_row." 条记录导入失败</br>";
				  $str .= "您的EXCEL数据：</br></br><table width=600 border=1 cellpadding=0 cellspacing=0 bordercolor=\"#CCCCCC\">";
				  $str .= $data_values;
				  $str .= "</table>";
				  
				}else{
					$str='请正确上传XLS文件';
				}
			}
		}
		include $this->template('web/import');
	}
	
	public function __web($f_name){
		global $_W,$_GPC;
		
		include_once  'web/hl'.strtolower(substr($f_name,5)).'.php';
	}


//以下不更新

	private function ProcessStatus() {
		global $_W;	
		$rule = pdo_fetchcolumn('SELECT status FROM '.tablename('broke_rule')." WHERE `weid` = :weid ",array(':weid' => $_W['uniacid']));	
		$status = array(
			'0'=>'推荐',
			'1'=>'跟进',
			'2'=>'无意向',
			
		);
		$b = explode(",", $rule);
		$status = array_merge($status, $b);
		return $status;
	}
	
	public function doWebUploadMusic() {
		global $_W;
		checklogin();
		if (empty($_FILES['imgFile']['name'])) {
			$result['message'] = '请选择要上传的音乐！';
			exit(json_encode($result));
		}

		if ($_FILES['imgFile']['error'] != 0) {
			$result['message'] = '上传失败，请重试！';
			exit(json_encode($result));
		}
		if ($file = $this->fileUpload($_FILES['imgFile'], 'music')) {
			if (!$file['success']) {
				exit(json_encode($file));
			}
			$result['url'] = $_W['config']['upload']['attachdir'] . $file['path'];
			$result['error'] = 0;
			$result['filename'] = $file['path'];
			exit(json_encode($result));
		}
	}

	private function fileUpload($file, $type) {
		global $_W;
		set_time_limit(0);
		$_W['uploadsetting'] = array();
		$_W['uploadsetting']['music']['folder'] = 'music';
		$_W['uploadsetting']['music']['extentions'] = array('mp3', 'wma', 'wav', 'amr');
		$_W['uploadsetting']['music']['limit'] = 50000;
		$result = array();
		$upload = file_upload($file, 'music');
		if (is_error($upload)) {
			message($upload['message'], '', 'ajax');
		}
		$result['url'] = $upload['url'];
		$result['error'] = 0;
		$result['filename'] = $upload['path'];
		return $result;
	}
	
	
	public function doMobileUserinfo() {
		global $_GPC,$_W;
		$weid = $_W['uniacid'];//当前公众号ID
		load()->func('communication');
		//用户不授权返回提示说明
		if ($_GPC['code']=="authdeny"){
		    $url = $_W['siteroot'].'app/'.$this->createMobileUrl('index', array(), true);
			header("location:$url");
			exit('authdeny');
		}
		//高级接口取未关注用户Openid
		if (isset($_GPC['code'])){
		    //第二步：获得到了OpenID
		    $appid = $_W['account']['key'];
		    $secret = $_W['account']['secret'];
			$serverapp = $_W['account']['level'];	
			if ($serverapp!=4) {
				$cfg = $this->module['config'];
			    $appid = $cfg['appid'];
			    $secret = $cfg['secret'];
				if(empty($appid) || empty($secret)){
					return ;
				}
			}
			$state = $_GPC['state'];
			//1为关注用户, 0为未关注用户
			
		    $rid = $_GPC['rid'];
			//查询活动时间
			$code = $_GPC['code'];
		    $oauth2_code = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$secret."&code=".$code."&grant_type=authorization_code";
		    $content = ihttp_get($oauth2_code);
		    $token = @json_decode($content['content'], true);
			if(empty($token) || !is_array($token) || empty($token['access_token']) || empty($token['openid'])) {
				echo '<h1>获取微信公众号授权'.$code.'失败[无法取得token以及openid], 请稍后重试！ 公众平台返回原始数据为: <br />' . $content['meta'].'<h1>';
				exit;
			}
		    $from_user = $token['openid'];
			//再次查询是否为关注用户
			$profile = pdo_fetch("select * from ".tablename('mc_mapping_fans')." where uniacid = ".$_W['uniacid']." and openid = '".$from_user."'");
			//关注用户直接获取信息	
			if ($profile['follow']==1){
			    $state = 1;
			}else{
				//未关注用户跳转到授权页
				$url = $_W['siteroot'].'app/'.$this->createMobileUrl('userinfo', array(), true);
				$oauth2_code = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".urlencode($url)."&response_type=code&scope=snsapi_userinfo&state=0#wechat_redirect";				
				header("location:$oauth2_code");
			}
			//未关注用户和关注用户取全局access_token值的方式不一样
			
			$access_token = $token['access_token'];
			$oauth2_url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$from_user."&lang=zh_CN";
			
			//使用全局ACCESS_TOKEN获取OpenID的详细信息			
			$content = ihttp_get($oauth2_url);
			$info = @json_decode($content['content'], true);
			if(empty($info) || !is_array($info) || empty($info['openid'])  || empty($info['nickname']) ) {
				echo '<h1>获取微信公众号授权失败[无法取得info], 请稍后重试！<h1>';
				exit;
			}
			if(!empty($profile)){
				$row = array(
					'uniacid' => $_W['uniacid'],
					'nickname'=>$info['nickname'],
					'avatar'=>$info['headimgurl'],
					'realname'=>$info['nickname']
				);
				pdo_update('mc_members', $row, array('uid'=>$profile['uid']));	
			}
			$oauth_openid = "brokeopenid".$_W['uniacid'];
			setcookie($oauth_openid, $info['openid'], time()+3600*240);
			$url = $this->createMobileUrl('index');
			//die('<script>location.href = "'.$url.'";</script>');
			header("location:$url");
			exit;
		}else{
			echo '<h1>网页授权域名设置出错!</h1>';
			exit;		
		}
	}
	
	private function CheckCookie() {
		global $_W;
		//return;
		$oauth_openid = "brokeopenid".$_W['uniacid'];
		if (empty($_COOKIE[$oauth_openid])) {
			$appid = $_W['account']['key'];
			$secret = $_W['account']['secret'];
			//是否为高级号
			$serverapp = $_W['account']['level'];	
			if ($serverapp!=4) {
				$cfg = $this->module['config'];
			    $appid = $cfg['appid'];
			    $secret = $cfg['secret'];
				if(empty($appid) || empty($secret)){
					return ;
				}
			}
			//借用的
			$url = $_W['siteroot'].'app/'.$this->createMobileUrl('userinfo', array(), true);
			$oauth2_code = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".urlencode($url)."&response_type=code&scope=snsapi_userinfo&state=0#wechat_redirect";				
			//exit($oauth2_code);
			header("location:$oauth2_code");
			
			exit;
		}
	}
	
}
/*
$url=$this->createMobileUrl('index');
			die('<script>location.href = "'.$url.'";</script>');
			header("location:$url");
			exit;
		*/
/**
 * 生成分页数据
 * @param int $currentPage 当前页码
 * @param int $totalCount 总记录数
 * @param string $url 要生成的 url 格式，页码占位符请使用 *，如果未写占位符，系统将自动生成
 * @param int $pageSize 分页大小
 * @return string 分页HTML
 */
function pagination1($tcount, $pindex, $psize = 15, $url = '', $context = array('before' => 5, 'after' => 4, 'ajaxcallback' => '')) {
	global $_W;
	$pdata = array(
		'tcount' => 0,
		'tpage' => 0,
		'cindex' => 0,
		'findex' => 0,
		'pindex' => 0,
		'nindex' => 0,
		'lindex' => 0,
		'options' => ''
	);
	if($context['ajaxcallback']) {
		$context['isajax'] = true;
	}

	$pdata['tcount'] = $tcount;
	$pdata['tpage'] = ceil($tcount / $psize);
	if($pdata['tpage'] <= 1) {
		return '';
	}
	$cindex = $pindex;
	$cindex = min($cindex, $pdata['tpage']);
	$cindex = max($cindex, 1);
	$pdata['cindex'] = $cindex;
	$pdata['findex'] = 1;
	$pdata['pindex'] = $cindex > 1 ? $cindex - 1 : 1;
	$pdata['nindex'] = $cindex < $pdata['tpage'] ? $cindex + 1 : $pdata['tpage'];
	$pdata['lindex'] = $pdata['tpage'];

	if($context['isajax']) {
		if(!$url) {
			$url = $_W['script_name'] . '?' . http_build_query($_GET);
		}
		$pdata['faa'] = 'href="javascript:;" onclick="p(\'' . $_W['script_name'] . $url . '\', \'' . $pdata['findex'] . '\', ' . $context['ajaxcallback'] . ')"';
		$pdata['paa'] = 'href="javascript:;" onclick="p(\'' . $_W['script_name'] . $url . '\', \'' . $pdata['pindex'] . '\', ' . $context['ajaxcallback'] . ')"';
		$pdata['naa'] = 'href="javascript:;" onclick="p(\'' . $_W['script_name'] . $url . '\', \'' . $pdata['nindex'] . '\', ' . $context['ajaxcallback'] . ')"';
		$pdata['laa'] = 'href="javascript:;" onclick="p(\'' . $_W['script_name'] . $url . '\', \'' . $pdata['lindex'] . '\', ' . $context['ajaxcallback'] . ')"';
	} else {
		if($url) {
			$pdata['faa'] = 'href="?' . str_replace('*', $pdata['findex'], $url) . '"';
			$pdata['paa'] = 'href="?' . str_replace('*', $pdata['pindex'], $url) . '"';
			$pdata['naa'] = 'href="?' . str_replace('*', $pdata['nindex'], $url) . '"';
			$pdata['laa'] = 'href="?' . str_replace('*', $pdata['lindex'], $url) . '"';
		} else {
			$_GET['page'] = $pdata['findex'];
			$pdata['faa'] = 'href="' . $_W['script_name'] . '?' . http_build_query($_GET) . '"';
			$_GET['page'] = $pdata['pindex'];
			$pdata['paa'] = 'href="' . $_W['script_name'] . '?' . http_build_query($_GET) . '"';
			$_GET['page'] = $pdata['nindex'];
			$pdata['naa'] = 'href="' . $_W['script_name'] . '?' . http_build_query($_GET) . '"';
			$_GET['page'] = $pdata['lindex'];
			$pdata['laa'] = 'href="' . $_W['script_name'] . '?' . http_build_query($_GET) . '"';
		}
	}

	$html = '<div class="pagination pagination-centered"><ul>';
	if($pdata['cindex'] > 1) {
		$html .= "<li><a {$pdata['faa']} class=\"pager-nav\">首页</a></li>";
		$html .= "<li><a {$pdata['paa']} class=\"pager-nav\">&laquo;上一页</a></li>";
	}
	//页码算法：前5后4，不足10位补齐
	if(!$context['before'] && $context['before'] != 0) {
		$context['before'] = 5;
	}
	if(!$context['after'] && $context['after'] != 0) {
		$context['after'] = 4;
	}

	if($context['after'] != 0 && $context['before'] != 0) {
		$range = array();
		$range['start'] = max(1, $pdata['cindex'] - $context['before']);
		$range['end'] = min($pdata['tpage'], $pdata['cindex'] + $context['after']);
		if ($range['end'] - $range['start'] < $context['before'] + $context['after']) {
			$range['end'] = min($pdata['tpage'], $range['start'] + $context['before'] + $context['after']);
			$range['start'] = max(1, $range['end'] - $context['before'] - $context['after']);
		}
		for ($i = $range['start']; $i <= $range['end']; $i++) {
			if($context['isajax']) {
				$aa = 'href="javascript:;" onclick="p(\'' . $_W['script_name'] . $url . '\', \'' . $i . '\', ' . $context['ajaxcallback'] . ')"';
			} else {
				if($url) {
					$aa = 'href="?' . str_replace('*', $i, $url) . '"';
				} else {
					$_GET['page'] = $i;
					$aa = 'href="?' . http_build_query($_GET) . '"';
				}
			}
			//$html .= ($i == $pdata['cindex'] ? '<li class="active"><a href="javascript:;">' . $i . '</a></li>' : "<li><a {$aa}>" . $i . '</a></li>');
		}
	}

	if($pdata['cindex'] < $pdata['tpage']) {
		$html .= "<li><a {$pdata['naa']} class=\"pager-nav\">下一页&raquo;</a></li>";
		$html .= "<li><a {$pdata['laa']} class=\"pager-nav\">尾页</a></li>";
	}
	$html .= '</ul></div>';
	return $html;
}
