<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$openid    = m('user')->getOpenid();
if (empty($openid)) {
    $openid = $_GPC['openid'];
}
$member  = m('member')->getMember($openid);
$uniacid = $_W['uniacid'];
$orderid = intval($_GPC['orderid']);
$xshordersn = $_GPC['xshordersn'];
$logid   = intval($_GPC['logid']);
$shopset = m('common')->getSysset('shop');
if ($_W['isajax']) {
    if (!empty($orderid)||!empty($xshordersn)) {
    	if(empty($xshordersn)){
        	$order = pdo_fetch("select * from " . tablename('ewei_shop_order') . ' where id=:id and uniacid=:uniacid and openid=:openid limit 1', array(
            	':id' => $orderid,
            	':uniacid' => $uniacid,
            	':openid' => $openid
       		));
        	$log = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
        			':uniacid' => $uniacid,
        			':module' => 'ewei_shop',
        			':tid' => $order['ordersn']
        	));
    	}
    	else
    	{
    		$order = pdo_fetchall("select * from " . tablename('ewei_shop_order') . ' where xshordersn=:xshordersn and uniacid=:uniacid and openid=:openid ', array(
    			':xshordersn' => $xshordersn,
    			':uniacid' => $uniacid,
    			':openid' => $openid
    		));
    		$xprice=0;
    		foreach($order as $rr=>$rows){
    			$ll = pdo_fetch('SELECT * FROM ' . tablename('core_paylog') . ' WHERE `uniacid`=:uniacid AND `module`=:module AND `tid`=:tid limit 1', array(
    				':uniacid' => $uniacid,
    				':module' => 'ewei_shop',
    				':tid' => $rows['ordersn']
    			));
    			$xprice=floatval($rows['price']+$xprice);
    		}
    		$log=$ll[0];
    	}
        if (empty($order)) {
            show_json(0, '订单未找到!');
        }
        if (!empty($log) && $log['status'] != '0') {
            show_json(0, '订单已支付, 无需重复支付!');
        }
        if(empty($xshordersn)){
        	$param_title     = $shopset['name'] . "订单: " . $order['ordersn'];
        }
        else
        {
        	$param_title     = $shopset['name'] . "总订单: " . $xshordersn;
        }
        $alipay          = array(
            'success' => false
        );
        $params          = array();
        $params['tid']   = empty($xshordersn)?$log['tid']:$xshordersn;
        $params['user']  = $openid;
        $params['fee']   = empty($xshordersn)?$log['fee']:$xprice;
        $params['title'] = $param_title;
        load()->func('communication');
        load()->model('payment');
        $setting = uni_setting($_W['uniacid'], array(
            'payment'
        ));
        if (is_array($setting['payment'])) {
            $options = $setting['payment']['alipay'];
            $alipay  = m('common')->alipay_build($params, $options, 0, $openid);
            if (!empty($alipay['url'])) {
                $alipay['success'] = true;
            }
        }
    } elseif (!empty($logid)) {
        $log = pdo_fetch('SELECT * FROM ' . tablename('ewei_shop_member_log') . ' WHERE `id`=:id and `uniacid`=:uniacid limit 1', array(
            ':uniacid' => $uniacid,
            ':id' => $logid
        ));
        if (empty($log)) {
            show_json(0, '充值出错!');
        }
        if (!empty($log['status'])) {
            show_json(0, '已经充值成功,无需重复支付!');
        }
        $alipay          = array(
            'success' => false
        );
        $params          = array();
        $params['tid']   = $log['logno'];
        $params['user']  = $log['openid'];
        $params['fee']   = $log['money'];
        $params['title'] = $log['title'];
        load()->func('communication');
        load()->model('payment');
        $setting = uni_setting($_W['uniacid'], array(
            'payment'
        ));
        if (is_array($setting['payment'])) {
            $options = $setting['payment']['alipay'];
            $alipay  = m('common')->alipay_build($params, $options, 1, $openid);
            if (!empty($alipay['url'])) {
                $alipay['success'] = true;
            }
        }
    }
    show_json(1, array(
        'alipay' => $alipay
    ));
}
include $this->template('order/pay_alipay');