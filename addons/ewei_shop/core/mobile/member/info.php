<?php
if (!defined('IN_IA')) {
    exit('Access Denied');
}
global $_W, $_GPC;
$openid = m('user')->getOpenid();
$member = m('member')->getInfo($openid);
if ($_W['isajax']) {
    if ($_W['ispost']) {
        $memberdata = $_GPC['memberdata'];
       pdo_update('ewei_shop_member', $memberdata, array(
                'openid' => $openid,
                'uniacid' => $_W['uniacid']
            ));
            $fuid=pdo_fetchcolumn("select `uid` from ".tablename("mc_mapping_fans")." where uniacid=:uniacid and openid=:openid ",array(":uniacid"=>$_W['uniacid'],":openid"=>$openid));
			if (!empty($fuid)) {
                $mcdata = $_GPC['mcdata'];
				//$mcdata['pwd']=$mcdata['password'];
				//$mcdata['uid']=intval($fuid);
                if(!empty($mcdata['password']))
                {
                    $salt=random(8);
                    $mcdata['password']=md5($salt.$mcdata['password']);
		    $mcdata['salt']=$salt;
                }
                load()->model('mc');
                mc_update(intval($fuid), $mcdata);
            }
        show_json(1);
    }
    show_json(1, array(
        'member' => $member
    ));
}
include $this->template('member/info');